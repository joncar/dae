BEGIN

DECLARE id INT DEFAULT 0;
DECLARE value TEXT;
DECLARE occurance INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
DECLARE splitted_value VARCHAR(255);
DECLARE done INT DEFAULT 0;
DECLARE cur1 CURSOR FOR SELECT colaboradores.id, colaboradores.fotos FROM colaboradores;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

DROP TEMPORARY TABLE IF EXISTS fotos_temp;
CREATE TEMPORARY TABLE fotos_temp(
`id` INT NOT NULL,
`value` VARCHAR(255) NOT NULL
) ENGINE=Memory;

OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO id, value;
    IF done THEN
      LEAVE read_loop;
    END IF;

    SET occurance = (SELECT LENGTH(value) - LENGTH(REPLACE(value,bound, ''))+1);
    SET i=1;
    WHILE i <= occurance DO
      SET splitted_value = (SELECT REPLACE(SUBSTRING(SUBSTRING_INDEX(value, bound, i),
      LENGTH(SUBSTRING_INDEX(value, bound, i - 1)) + 1), ',', ''));

      INSERT INTO fotos_temp VALUES (id, splitted_value);
      INSERT INTO colaboradores_fotos VALUES (NULL,id,splitted_value,'');
      SET i = i + 1;

    END WHILE;
  END LOOP;

  SELECT * FROM fotos_temp;
 CLOSE cur1;
 END