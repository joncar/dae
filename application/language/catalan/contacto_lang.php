<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['success_response_contact_form'] = 'Gràcies per contactar-nos, en breu ens comunicarem amb vostè.';
$lang['error_response_contact_form'] = 'Si us plau completi les dades sol·licitades  <script>$("#guardar").attr("disabled",false); </script>';
$lang['error_response_captcha_form'] = 'Captcha introduït incorrectament';