<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); $lang['Avís legal'] = 'Avís legal';$lang['inici'] = 'inici';$lang['aviso_legal_texto'] = '<h4 class="mt-5 text-theme-colored">LLEI DELS SERVEIS DE LA SOCIETAT DE LA INFORMACIÓ (LSSI)</h4>
                                <p>DONES AMB EMPENTA, responsable del lloc web, d&#039ara endavant RESPONSABLE, posa a disposició
                                dels usuaris el present document, amb el qual pretén donar compliment a les obligacions disposades
                                en la Llei 34/2002, de 11 de juliol, de serveis de la Societat de la Informació i de Comerç Electrònic
                                (LSSICE), BOE Nº 166, així com informar a tots els usuaris del lloc web respecte a quines són les
                                condicions d&#039ús.</p>
                                <p>Tota persona que accedeixi a aquest lloc web assumeix el paper d&#039usuari, comprometent-se a
                                l&#039observança i compliment rigorós de les disposicions aquí disposades, així com a qualsevol altra
                                disposició legal que fora d&#039aplicació.</p>
                                <p>DONES AMB EMPENTA es reserva el dret de modificar qualsevol tipus d&#039informació que pogués
                                aparèixer al lloc web, sense que hi hagi obligació de preavisar o posar en coneixement dels usuaris
                                aquestes obligacions, entenent-se com suficient amb la publicació al lloc web de DONES AMB
                                EMPENTA.</p>
                                 <h4 class="mt-5 text-theme-colored">1. DADES IDENTIFICATIVES</h4>
                                              <p>Nom de domini: www.dae.cat<br>
                                Nom comercial:<br>
                                Denominació social: DONES AMB EMPENTA<br>
                                NIF: G61266649<br>
                                Domicili social: Clos, 42, 2º 8700 IGUALADA<br>
                                Telèfon: 938048265 NATI 93804<br>
                                Correu electrònic: donesambempenta@dae.cat<br>
                                Inscrita en el Registre (Mercantil / Públic): Asociaciones de la Generalitat, con el número 19.174</p>
                                              <h4 class="mt-5 text-theme-colored">2. DRETS DE PROPIETAT INTEL·LECTUAL I INDUSTRIAL</h4>
                                              <p>El lloc web, incloent a títol enunciatiu però no limitatiu la seva programació, edició, compilació i altres
                                elements necessaris per al seu funcionament, els dissenys, logotips, text i / o gràfics, són propietat del
                                RESPONSABLE o, si és el cas, disposa de llicència o autorització expressa per part dels autors. Tots
                                els continguts del lloc web es troben degudament protegits per la normativa de propietat intel·lectual i
                                industrial, així com inscrits en els registres públics corresponents.</p>
                                <p>Independentment de la finalitat per a la qual fossin destinats, la reproducció total o parcial, ús,
                                explotació, distribució i comercialització, requereix en tot cas de l&#039autorització escrita prèvia per part del
                                RESPONSABLE. Qualsevol ús no autoritzat prèviament es considera un incompliment greu dels drets
                                de propietat intel·lectual o industrial de l&#039autor.</p>
                                <p>Els dissenys, logotips, text i / o gràfics aliens al RESPONSABLE i que poguessin aparèixer en el lloc
                                web, pertanyen als seus respectius propietaris, sent ells mateixos responsables de qualsevol possible
                                controvèrsia que pogués suscitar respecte als mateixos. El RESPONSABLE autoritza expressament a que tercers puguin redirigir directament als continguts concrets del lloc web, i en tot cas redirigir al lloc
                                web principal de www.dae.cat.</p>
                                <p>El RESPONSABLE reconeix a favor dels seus titulars els corresponents drets de propietat intel·lectual i
                                industrial, no implicant el seu sol esment o aparició en el lloc web l&#039existència de drets o responsabilitat
                                alguna sobre els mateixos, com tampoc respatller, patrocini o recomanació per part del mateix.</p>
                                <p>Per realitzar qualsevol tipus d&#039observació respecte a possibles incompliments dels drets de propietat
                                intel·lectual o industrial, així com sobre qualsevol dels continguts del lloc web, pot fer-ho a través del
                                correu electrònic donesambempenta@dae.cat.</p>
                                              <h4 class="mt-5 text-theme-colored">3. EXEMPCIÓ DE RESPONSABILITATS</h4>
                                              <p>El RESPONSABLE s&#039eximeix de qualsevol tipus de responsabilitat derivada de la informació publicada
                                al seu lloc web sempre que aquesta informació hagi estat manipulada o introduïda per un tercer aliè a
                                aquest.</p>
                                              <h4 class="mt-5 text-theme-colored">Ús de Cookies</h4>
                                              <p>Aquest lloc web de pot utilitzar cookies tècniques (petits arxius d&#039informació que el servidor envia a
                                l&#039ordinador de qui accedeix a la pàgina) per dur a terme determinades funcions que són considerades
                                imprescindibles per al correcte funcionament i visualització del lloc. Les cookies utilitzades tenen, en tot
                                cas, caràcter temporal, amb l&#039única finalitat de fer més eficaç la navegació, i desapareixen en acabar la
                                sessió de l&#039usuari. En cap cas, aquestes cookies proporcionen per si mateixes dades de caràcter
                                personal i no s&#039utilitzaran per a la recollida de les mateixes.</p>
                                              <p>Mitjançant l&#039ús de cookies també és possible que el servidor on es troba la web reconegui el navegador
                                utilitzat per l&#039usuari amb la finalitat que la navegació sigui més senzilla, permetent, per exemple, l&#039accés
                                dels usuaris que s&#039hagin registrat prèviament a les àrees, serveis, promocions o concursos reservats
                                exclusivament a ells sense haver de registrar en cada visita. També es poden utilitzar per mesurar
                                l&#039audiència, paràmetres de trànsit, controlar el progrés i nombre d&#039entrades, etc, sent en aquests casos
                                cookies prescindibles tècnicament però beneficioses per a l&#039usuari. Aquest lloc web no s&#039instal·larà
                                cookies prescindibles sense el consentiment previ de l&#039usuari.</p>
                                <p>L&#039usuari té la possibilitat de configurar el seu navegador per ser alertat de la recepció de cookies i per
                                impedir la seva instal·lació al seu equip. Si us plau, consulti les instruccions del seu navegador per
                                ampliar aquesta informació.</p>
                                <h4 class="mt-5 text-theme-colored">Política d&#039enllaços</h4>
                                <p>Des del lloc web, és possible que es redirigeixi a continguts de tercers llocs web. Atès que el
                                RESPONSABLE no pot controlar sempre els continguts introduïts pels tercers en els seus respectius
                                llocs web, no assumeix cap tipus de responsabilitat respecte a aquests continguts. En tot cas,
                                procedirà a la retirada immediata de qualsevol contingut que pogués contravenir la legislació nacional o
                                internacional, la moral o l&#039ordre públic, procedint a la retirada immediata de la redirecció a dit lloc web,
                                posant en coneixement de les autoritats competents el contingut en qüestió.</p>
                                <p>El RESPONSABLE no es fa responsable de la informació i continguts emmagatzemats, a títol
                                enunciatiu però no limitatiu, en fòrums, xats, generadors de blogs, comentaris, xarxes socials o
                                qualsevol altre mitjà que permeti a tercers publicar continguts de forma independent en la pàgina web
                                del RESPONSABLE. No obstant això, i en compliment del que disposen els articles 11 i 16 de la
                                LSSICE, es posa a disposició de tots els usuaris, autoritats i forces de seguretat, col·laborant de forma
                                activa en la retirada o, si escau, bloqueig de tots aquells continguts que puguin afectar o contravenir la
                                legislació nacional o internacional, els drets de tercers o la moral i l&#039ordre públic. En cas que l&#039usuari
                                consideri que existeix en el lloc web algun contingut que pogués ser susceptible d&#039aquesta classificació,
                                es prega ho faci saber de forma immediata a l&#039administrador del lloc web.</p>
                                <p>Aquest lloc web ha estat revisat i provat perquè funcioni correctament. En principi, pot garantir-se el
                                correcte funcionament els 365 dies de l&#039any, 24 hores al dia. No obstant això, el RESPONSABLE no
                                descarta la possibilitat que existeixin certs errors de programació, o que esdevinguin causes de força
                                major, catàstrofes naturals, vagues o circumstàncies semblants que facin impossible l&#039accés a la
                                pàgina web.</p>
                                <h4 class="mt-5 text-theme-colored">Adreces IP</h4>
                                <p>Els servidors del lloc web podran detectar de manera automàtica l&#039adreça IP i el nom de domini
                                utilitzats per l&#039usuari. Una adreça IP és un nombre assignat automàticament a un ordinador quan
                                aquest es connecta a Internet. Tota aquesta informació és registrada en un fitxer d&#039activitat del servidor
                                degudament inscrit que permet el posterior processament de les dades amb la finalitat d&#039obtenir
                                mesures únicament estadístiques que permetin conèixer el nombre d&#039impressions de pàgines, el
                                nombre de visites realitzades als servidors web, l&#039ordre de visites, el punt d&#039accés, etc.</p>
                                <h4 class="mt-5 text-theme-colored">4. Llei aplicable i jurisdicció</h4>
                                <p>Per a la resolució de totes les controvèrsies o qüestions relacionades amb el present lloc web o de les
                                activitats en ell desenvolupades, serà d&#039aplicació la legislació espanyola, a la qual se sotmeten
                                expressament les parts, sent competents per a la resolució de tots els conflictes derivats o relacionats
                                amb el seu ús els Jutjats i Tribunals més propers a IGUALADA.</p>';$lang['Resultado de busqueda'] = 'Resultado de busqueda';$lang['Mès informaciò'] = 'Mès informaciò';$lang['Collaboracions'] = 'Col·laboracions';$lang['Concurs_Fotografia'] = 'Concurs Fotografia';$lang['Hemeroteca'] = 'Hemeroteca';$lang['Concurs_Fotografic'] = 'Concurs Fotogràfic';$lang['Contacto'] = 'Contacta&#039ns';$lang['Contacte'] = 'Contacte';$lang['Posacontacte_amb_nosaltres'] = 'Posa&#039t en contacte amb nosaltres';$lang['Necesita_Informacion'] = 'Si tens qualsevol dubte o necessites informació sobre Dones amb Empenta
                            <br>
                            no dubtis en contactar-nos  a través del formulari, telèfon o correu electrònic.';$lang['Colaboracion'] = 'Col·labora i/o ';$lang['amb_nosaltres'] = 'Treballa amb nosaltres ';$lang['ofertes_treball'] = 'Veure ofertes de treball';$lang['Política_Cookies'] = 'Política de Cookies';$lang['Cura_professionals'] = 'Cura de les professionals';$lang['com_ho_fem'] = 'com ho fem ';$lang['compromís_DAE'] = 'El compromís de DAE';$lang['Cuidem'] = 'Cuidem de qui cuida';$lang['persones_cuidem'] = 'Som persones que treballem per persones. Disposem de supervisió externa de casos i equips per garantir el benestar i una millor intervenció professional. D’un servei de prevenció de riscos laborals i d’un pla d’equitat de gènere per garantir la igualtat efectiva i la no discriminació. Millorem les condicions laborals respecte al conveni col·lectiu del sector. I gaudim d’una borsa d’hores de formació continuada amb cursos a mida.';$lang['Veure'] = 'Veure';$lang['ens_mou'] = 'Què ens mou';$lang['Dones_Empenta'] = 'Dones amb Empenta';$lang['Dones_amb_empenta'] = 'Dones Amb Empenta és una entitat oberta a les inquietuds i necessitats de les persones. Treballem per a generar canvis en la nostra realitat i construir una societat equitativa i lliure de violències masclistes.

                        <br>Tenim reconeguda la Declaració d’Utilitat Pública i garantim la màxima qualitat de les nostres activitats i serveis. Treballem amb rigor, de forma eficaç i eficient, per donar a la ciutadania una resposta integral, especialitzada, àgil, propera i coordinada. 

                        Treballem en xarxa i participem en els espais de construcció i reivindicació del teixit associatiu. Formem part del Consell Nacional de Dones de Catalunya.';$lang['Desigualtats'] = 'Desigualtats i Violències';$lang['Treballem_experiencia'] = 'Treballem a partir de l’experiència que la societat patriarcal ens impedeix desenvolupar-nos amb drets i llibertats plenes. Cal aconseguir una societat més justa i sostenible, posant en el centre la vida i en valor la diversitat, la cura, el plaer, la cooperació. 

                                    <br><br>Avancem per a l’eradicació de les violències masclistes mitjançant programes i serveis de formació, sensibilització, prevenció, detecció, atenció i recuperació  del dany sofert. El nostre abordatge és transversal, integral i multidisciplinar. Tenim present la dimensió social i estructural però també la subjectiva, les necessitats i els objectius de cada persona.';$lang['PERSPECTIVA_GÈNERE'] = 'PERSPECTIVA DE GÈNERE';$lang['mirada_amplia'] = ' La nostra és una mirada àmplia i feminista. 

                                    <br><br>Tots el nostres projectes i serveis són sensibles a les diferents formes en que les persones experimenten les situacions: mandats i creences, necessitats, oportunitats i limitacions, en funció de la seva socialització de gènere.
';$lang['APODERAMENT'] = 'APODERAMENT';$lang['Creiem_acompanyament'] = 'Creiem en un acompanyament professional que parteix de les potencialitats. La identificació i l’enfortiment de les competències personals esdevenen clau en els processos d’apoderament de les persones. 

                                <br><br>Són imprescindibles la privacitat i el respecte a les individualitats, ritmes i  decisions de cada persona.  

                                <br><br>Donem visibilitat i valor al treball de sosteniment de la vida, el treball de cures, que entenem com a responsabilitat col·lectiva.';$lang['INTERSECCIONALITAT'] = 'INTERSECCIONALITAT';$lang['diversitat'] = 'Tenim en compte la diversitat de les persones, la complexitat de les situacions  i les diferents variables que la conformen. El gènere, la diversitat sexual, l’edat, l’origen, el nivell socioeconòmic, l’estat de salut específic, entre d’altres, que fan necessària una atenció individualitzada. Incorporem el repte de la mirada a la diversitat identificant i desconstruint el nostres propis prejudicis, per tal d’evitar reproduir-los en la nostra tasca.

                                    <br><br>La sensibilitat a la diversitat, connecta amb la voluntat de generar espais de seguretat on les persones es puguin expressar, proveint l&#039oportunitat de reconèixer-se a elles mateixes i escapant de ser objectivitzades, com a primer pas cap a l&#039apoderament.';$lang['INNOVACIO'] = 'INNOVACIÓ';$lang['valtreballar'] = 'No ens val treballar bé, sinó que volem fer-ho sempre millor. 

                                    <br><br>Garantim la formació i actualització professional continua de les nostres  professionals. La metodologia de treball potencia la creativitat dels equips i incorpora la innovació per a una millor resposta a les demandes i necessitats de la ciutadania.

                                    <br><br>Tenim capacitat per a crear nous serveis i per a adaptar-nos a col·lectius i territoris. ';$lang['COMUNITAT'] = 'COMUNITAT';$lang['trobada_dones'] = 'Vam començar al 1996 com a espai de trobada de dones. Al llarg dels anys hem ampliat els serveis professionals i diversificat els col·lectius als que ens adrecem. Però continuem fidels al nostre origen i promovem la participació i la dinamització del teixit social i associatiu. Només amb la implicació de tots i totes podrem assolir una societat millor.
                                         <br><br>El treball en xarxa és per a nosaltres un pilar bàsic en la intervenció professional i impulsem aquesta metodologia en tots els nostres serveis. ';$lang['Qui_som'] = 'Qui som';$lang['home'] = 'home';$lang['D_A_E'] = 'Dones Amb Empenta (DAE) és una associació oberta a les inquietuds i necessitats de les dones, un espai per treballar i generar canvis en la nostra realitat.
                            <br>
                            L’entitat, nascuda el 1996 com un espai de trobada per a dones, ha anat ampliant progressivament els seus serveis i ha diversificat el públic a qui s’adreça  adaptant-se a les necessitats emergents. Actualment, DAE treballa mitjançant programes de sensibilització  i prevenció social, atenció i  formació dirigit a dones, joves, famílies i professionals de diferents àmbits interessats en introduir en els seus equips la perspectiva de gènere.
                            <br>';$lang['Objectius'] = 'Objectius';$lang['objectiu_general'] = 'L’objectiu general de l’entitat està orientat a transformar la realitat social més propera en clau de perspectiva de gènere generant accions i recursos que garanteixin el dret de les dones a viure una vida lliure de violència i en garantia de drets.
                                    <br>
                                    • Combatre  la violència masclista mitjançant la sensibilització, la prevenció, la detecció, l’atenció a les dones i als seus fills i filles, així com  la recuperació del dany sofert.
                                    <br>
                                    • Promoure accions per fer efectiva la igualtat d’oportunitats.
                                    <br>
                                    • Acompanyar i donar suport a la diversitat de les famílies, les situacions de canvi i la parentalitat positiva.
                                    <br>
                                    • Oferir espais de trobada, de reflexió i d’intercanvi d’experiències en diferents àmbits: educatiu, sanitari, administratiu, i adreçats a diferents col·lectius: famílies, personal tècnic, professionals, infants i joves.
                                    <br>
                                    • Promoure la participació de les dones en el teixit associatiu del territori.';$lang['Missio'] = 'Missió';$lang['articular_acciones'] = ' Articular accions que ens permetin transformar la realitat des d’una perspectiva feminista a partir de l’atenció, la prevenció, la sensibilització social i la formació.';$lang['Visio'] = 'Visió';$lang[' serveis_atencio'] = 'Volem ser entitat de referència al territori en la prevenció, detecció i abordatge de les diferents formes de violències masclistes així com en la creació i gestió de serveis d’atenció a les dones i a les seves famílies. ';$lang['Valors'] = 'Valors';$lang['valors_feminista'] = 'Els valors que mouen la nostra acció són la llibertat, el treball en xarxa, la reflexió sobre la tasca, l’acompanyament des de les potencialitats i una mirada Àmplia amb perspectiva feminista.';$lang['lista_valors_feminista'] = '<br>

                                Creiem en un acompanyament significatiu que parteix de les potencialitats. La identificació i   l’enfortiment de les competències d’autonomia personal i social  esdevenint clau en els processos d’empoderament de les dones i el seu desenvolupament en condicions de  llibertat.
                                <br>
                                Considerem imprescindible el respecte a les individualitats, els ritmes i les decisions de les persones amb les que treballem sobre les seves pròpies vides.
                                <br> 
                                Incorporem a la nostra reflexió el repte de la mirada intercultural intentant identificar i deconstruir el nostres propis prejudicis per tal d’evitar reproduir-los en el dia a dia de la nostra tasca.
                                <br> 
                                Volem tenir en compte la complexitat de les situacions que atenem i en les diferents variables que la conformen, des d’una perspectiva Interseccional. D’aquesta manera el gènere, la preferència sexual, l’edat o l’origen, entre d’altres, fan de cada experiència una situació única que hem d’atendre i valorar en funció del context.
                                <br>
                                Ens nodrim de l’experiència de les diferents entitats del territori i ens posem també a disposició compartint els sabers i fent del treball en xarxa un pilar bàsic i identitari de l’entitat. ';$lang['Com ho fem'] = 'Com ho fem';$lang['Tots'] = 'Tots';$lang['cookies_text'] = '<h4 class="mt-5 text-theme-colored">INFORMACIÓ SOBRE COOKIES</h4>
                    <p>A causa de l&#039entrada en vigor de la referent modificació de la "Llei de Serveis de la Societat de la Informació" (LSSICE)
                    establerta pel Reial Decret 13/2012, és d&#039obligació obtenir el consentiment exprés de l&#039usuari de totes les pàgines web que
                    utilitzen cookies prescindibles, abans que aquest navegui per elles.</p>

                                 <h4 class="mt-5 text-theme-colored">QUÈ SÓN LES COOKIES?</h4>
                    <p>Les cookies i altres tecnologies similars tals com a local shared objects, flash cookies o píxels, són eines emprades pels
                    servidors web per emmagatzemar i recuperar informació sobre els seus visitants, així com per oferir un correcte
                    funcionament del lloc.</p>
                    <p>Mitjançant l&#039ús d&#039aquests dispositius es permet al servidor web recordar algunes dades concernents a l&#039usuari, com les
                    seves preferències per a la visualització de les pàgines d&#039aquest servidor, nom i contrasenya, productes que més li
                    interessen, etc.</p>
                                  <h4 class="mt-5 text-theme-colored">COOKIES AFECTADES PER LA NORMATIVA I COOKIES EXEMPTES</h4>
                    <p>Segons la directiva de la UE, les cookies que requereixen el consentiment informat per part de l&#039usuari són les cookies
                    d&#039analítica i les de publicitat i afiliació, quedant exceptuades les de caràcter tècnic i les necessàries per al funcionament del
                    lloc web o la prestació de serveis expressament demanats per l&#039usuari.</p>

                    <h4 class="mt-5 text-theme-colored">QUIN TIPUS DE COOKIES HI HAN?</h4>
                    <p>Sobre els tipus de galetes, hi ha cinc grans grups:<br>
                    •  Cookies analítiques: recullen informació de l&#039ús que es realitza del lloc web.<br>
                    • Cookies socials: són aquelles necessàries per a xarxes socials externes.<br>
                    • Cookies d&#039afiliats: permeten fer un seguiment de les visites procedents d&#039altres webs, amb les que el lloc web
                    estableix un contracte d‘afiliació (empreses d&#039afiliació).<br>
                    • Cookies de publicitat i comportament: recullen informació sobre les preferències i eleccions personals de
                    l&#039usuari (retargeting).<br>
                    • Cookies tècniques i funcionals: són les estrictament necessàries per a l&#039ús del lloc web i per a la prestació del
                    servei contractat.</p>
                  <h4 class="mt-5 text-theme-colored">COOKIES QUE UTILITZEM EN EL NOSTRE ESPAI WEB</h4>
                  <p>• Llistar els noms de les cookies i les seves funcions, per exemple:</p>
                  <p>• PHPSESSID: cookie tècnica i estrictament necessària que conté l&#039identificador de la sessió. S&#039elimina en tancar
                  el navegador.</p>
                  <p>• _lang: cookie tècnica i estrictament necessària que conté l&#039idioma de la sessió. S&#039elimina en tancar el
                  navegador.</p>
                  <p>• ac_cookies: cookie tècnica i estrictament necessària que conté el valor de si s&#039ha acceptat la instal·lació de
                  cookies. Caduca en 1 any des de l&#039última actualització.</p>
                  <p>• _ga: cookie de Google Analytics Universal que habilita la funció de control de visites úniques. La primera
                  vegada que un usuari entri al lloc web a través d&#039un navegador s&#039instal·larà aquesta galeta. Quan aquest usuari
                  torni a entrar a la web amb el mateix navegador, la galeta considerarà que és el mateix usuari. Només en el cas
                  que l&#039usuari canviï de navegador, es considerarà un altre usuari. Caduca als 2 anys des de l&#039última actualització.</p>
                  <p>• _gat: Aquesta cookie s&#039associa amb Google Analytics Universal. S&#039utilitza per limitar la velocitat de petició - la
                  limitació de la recollida de dades en els llocs d&#039alt trànsit. Caduca als 10 minuts.</p>
                  <p>• _utma: cookie de Google Analytics que registra la data de la primera i última vegada que l&#039usuari va vestir el lloc
                  web. Caduca als 2 anys des de l&#039última actualització.</p>
                  <p>• _utmb: cookie de Google Analytics que registra l&#039hora d&#039arribada a la pàgina web. Caduca als 30 minuts des de
                  l&#039última actualització.</p>
                  <p>• _utmc: cookie de Google Analytics utilitzada per a la interoperabilitat amb el codi de seguiment urchin.js.
                  S&#039elimina en tancar el navegador.</p>
                  <p>• _utmt: cookie de Google Analytics utilitzada per a processar el tipus de sol·licitud demanada per l&#039usuari.
                  Caduca en finalitzar la sessió.</p>
                  <p>• _utmv: cookie de Google Analytics utilitzada per a segmentar dades demogràfiques. Caduca en finalitzar la
                  sessió.</p>
                  <p>• _utmz: cookie de Google Analytics que emmagatzema la font de trànsit o una campanya per explicar com
                  l&#039usuari va arribar al lloc web. Caduca als 6 mesos des de l&#039última actualització.</p>
                        <h4 class="mt-5 text-theme-colored">REVOCACIÓ DEL CONSENTIMENT PER INSTAL·LAR COOKIES
                        COM ELIMINAR LES COOKIES DEL NAVEGADOR</h4>
                  <p><b>Chrome</b></p>
                  <p>1. Seleccioneu la icona d&#039Eines</p>
                  <p>2. Feu clic a Configuració.</p>
                  <p>3. Fes clic a Mostra Opcions Avançades.</p>
                  <p>4. A la secció "Privadesa" feu clic a Configuració de contingut.
                  <br>• Suprimeix les galetes: Fes clic a Totes les galetes i dades del lloc ...
                  <br>• No permetre que s&#039emmagatzemin cookies.</p>
                  <p>5. Fes clic a Elimina les dades de navegació (buidar la Memòria cau).</p>
                  <p>6. Tanca i reinicia el navegador.</p>
                  <p>Per a més informació sobre Chrome premi aquí:
                  http://support.google.com/chrome/answer/95647?hl=es</p>
                  <p><b>Internet Explorer. Versió 11</b></p>
                  <p>1. Selecciona Eines | Opcions d&#039Internet.</p>
                  <p>2. Fes clic a la pestanya General.</p>
                  <p>3. A la secció "Historial de navegació", feu clic a Suprimeix l&#039historial d&#039exploració al sortir.</p>
                  <p>4. Seleccionar Eliminar arxius.</p>
                  <p>5. Seleccionar Suprimeix les galetes.</p>
                  <p>6. Feu clic a Suprimeix.</p>
                  <p>7. Fes clic a D&#039acord.</p>
                  <p>8. Tanca i reinicia el navegador.</p>
                  <p>Per a més informació sobre Internet Explorer premi aquí:
                  http://windows.microsoft.com/es-419/windows7/how-to-manage-cookies-in-internet-explorer-9</p>
                  <p><b>Firefox. Versió 18</b></p>
                  <p>1. Selecciona Firefox | Historial | Neteja l&#039historial recent.</p>
                  <p>2. Al costat de "Detalls", fes clic a la fletxa cap avall.</p>
                  <p>3. Selecciona les següents caselles de verificació: galetes, Memòria cau, Inicis de sessió actius</p>
                  <p>4. Usant el "Període de temps per esborrar" al menú desplegable, selecciona Tot.</p>
                  <p>5. Feu clic a Neteja ara.</p>
                  <p>6. Tanca i reinicia el navegador.</p>
                  <p>Pot acceptar o rebutjar les galetes individualment en les Preferències de Firefox, a la secció Historial disponible a Eines>
                  Opcions> Privadesa.</p>
                  <p>Per a més informació sobre Mozilla Firefox premi aquí:
                  https://www.mozilla.org/es-ES/privacy/websites/#cookies</p>
                  <p><b>Safari Versió 5.1</b></p>
                  <p>1. Seleccioneu la icona de Safari / Edita | Restaura Safari.</p>
                  <p>2. Selecciona les següents caselles de verificació: Esborrar l&#039historial, Eliminar totes les dades de lloc web</p>
                  <p>3. Fes clic a Restablir.</p>
                  <p>4. Tanca i reinicia el navegador.</p>
                  <p>Per a més informació sobre Safari premi aquí:
                  http://support.apple.com/kb/PH5042</p>
                  <p><b>Opera</b></p>
                  <p>Opcions - Avançat - Galetes.</p>
                  <p>Les opcions de cookies controlen la manera en què Opera els maneja i per tant la seva acceptació o rebuig.</p>
                  <p>Per a més informació sobre Òpera premi aquí:
                  http://help.opera.com/Linux/10.60/es-ES/cookies.html</p>
                  <p><b>Altres navegadors</b></p>
                  <p>Consulteu la documentació del navegador que tingui instal·lat.</p>';$lang['Territori'] = 'Territori';$lang['territoriactuacio'] = 'Territori
                                            <br>
                                            d&#039actuació
                                            <br>';$lang['territoriactuaciotext'] = 'El territori d’actuació de Dones Amb Empenta és Catalunya, essent les comarques de la Catalunya Central i de Lleida el seu espai de màxima influència. La seu de l’entitat es troba a Igualada.';$lang['puntoatencio'] = 'punt d&#039atenció i activitat';$lang['Directora'] = 'Directora';$lang['Perfil'] = 'Perfil';$lang['lentitat'] = 'L&#039entitat en xifres';$lang['lesxifres'] = 'Les xifres de Dones amb Empenta';$lang['elsfan'] = 'Els números ens fan créixer';$lang['elsfanstext'] = 'Treballem per a assolir els nostre objectius i poder arribar a més persones.';$lang['nprofesionales'] = 'Nº de professionals 2017';$lang['volum'] = 'Volum econòmic 2017';$lang['Persones ateses 2017'] = 'Persones ateses 2017';$lang['Ndprofessionals2016'] = 'Nº de professionals 2016';$lang[' Volum economic 2016'] = ' Volum econòmic 2016';$lang['Persones ateses 2016'] = 'Persones ateses 2016';$lang['Lequip'] = 'L&#039equip';$lang['Qui som'] = 'Qui som';$lang['veurecurriculum'] = 'Veure currículum professional';$lang['Collaboraio'] = 'Col·labora i/o';$lang['Treballa amb nosaltres'] = 'Treballa amb nosaltres';$lang['Veure ofertes de treball'] = 'Veure ofertes de treball';$lang['Actuem i Transformem'] = 'Actuem i Transformem';$lang['Treballem per a generar'] = 'Treballem per a generar';$lang['canvis en la nostra realitat'] = 'canvis en la nostra realitat.';$lang['canvis_text'] = 'Dissenyem, gestionem i desenvolupem serveis d’atenció i suport a dones, <br>joves i famílies, i innovem amb programes de prevenció i sensibilització social, <br>per a la transformació cap a una societat més sostenible i justa, sense <br>desigualtats per raons de gènere.';$lang['llegir més'] = 'llegir més';$lang['SABEM I INNOVEM'] = 'SABEM I INNOVEM';$lang['Tenim experiencia'] = 'Tenim experiència';$lang['i la volem compartir'] = 'i la volem compartir';$lang['ilabolemtext'] = ' Som entitat de referència. Oferim formació, assessorament <br>
                                    i consultoria professional. Generem continguts i processos <br>
                                    d’atenció a partir de la pròpia experiència i del treball en <br>
                                    xarxa amb la transferència de sabers. Impulsem nous espais <br>
                                    d’actuació i col·laborem amb iniciatives comunitàries.';$lang['LAssociacio'] = 'L&#039Associació';$lang['Dones amb empenta'] = 'Dones amb empenta';$lang['elfeminisme'] = '“El feminisme és la idea radical que sosté que les dones som persones” <br>Angela Davis.';$lang['La nostra historia'] = 'La nostra història';$lang['Memoria 2017'] = 'Memòria 2017';$lang['DESIGUALTATS I VIOLENCIES'] = 'DESIGUALTATS I VIOLÈNCIES';$lang['desitext'] = 'Treballem per aconseguir una societat més justa i sostenible, posant en el centre la vida i en valor la diversitat, la cura, el plaer, la cooperació.';$lang['PERSPECTIVA DE GENERE'] = 'PERSPECTIVA DE GÈNERE';$lang['perspective'] = 'Tots el nostres projectes i serveis són sensibles a les diferents formes en que les persones experimenten les situacions.';$lang['apotext'] = 'Donem visibilitat i valor al treball de sosteniment de la vida, el treball de cures, que entenem com a responsabilitat col·lectiva.';$lang['intertext'] = 'Tenim en compte la diversitat de les persones, la complexitat de les situacions  i les diferents variables que la conformen.';$lang['inotext'] = 'Garantim la formació i actualització professional continua de les nostres  professionals.';$lang['comutext'] = 'Promovem la participació i la dinamització del teixit social i associatiu. Només amb la implicació de tots i totes podrem assolir una societat millor.';$lang['QUÈ FEM'] = 'QUÈ FEM';$lang['dactuació'] = 'd&#039actuació';$lang['tertext'] = 'El territori d’actuació de Dones Amb Empenta és Catalunya, essent les comarques de la Catalunya Central i de Lleida el seu espai de màxima influència. La seu de l’entitat es troba a Igualada.';$lang['Allà on som'] = 'Allà on som';$lang['Lequip de treball'] = 'L&#039equip de treball';$lang['Coneix lequip'] = 'Coneix l&#039equip';$lang['Collabora io'] = 'Col·labora i/o';$lang['notícies'] = 'notícies';$lang['Veure totes les notícies'] = 'Veure totes les notícies';$lang['xarxes'] = 'xarxes';$lang['Totes'] = 'Totes';$lang['Política de Privacitat'] = 'Política de Privacitat';$lang['politext'] = '<h4 class="mt-5 text-theme-colored">1. INFORMACIÓ A L&#039USUARI</h4>
              <p>DONES AMB EMPENTA des d&#039ara RESPONSABLE, és el Responsable del tractament de les dades
personals de l&#039Usuari i l&#039informa que aquestes dades seran tractades de conformitat amb el que
disposen les normatives vigents en protecció de dades personals, el Reglament (UE) 2016/679 de 27
d&#039abril de 2016 (GDPR) relatiu a la protecció de les persones físiques pel que fa al tractament de dades
personals i a la lliure circulació d&#039aquestes dades pel que se li facilita la següent informació del
tractament:</p>
<p>Fi del tractament: mantenir una relació comercial amb l&#039Usuari.</p>
<h4 class="mt-5 text-theme-colored">Les operacions previstes per realitzar el tractament són:</h4>
<p>- Remissió de comunicacions comercials publicitàries per email, fax, SMS, MMS, comunitats socials o qualsevol altre mitjà
electrònic o físic, present o futur, que possibiliti realitzar comunicacions comercials. Aquestes comunicacions seran
realitzades pel RESPONSABLE i relacionades sobre els seus productes i serveis, o dels seus col·laboradors o proveïdors
amb els que aquest hagi arribat a algun acord de promoció. En aquest cas, els tercers mai tindran accés a les dades
personals.</p>
<p>- Realitzar estudis estadístics.</p>
<p>- Tramitar encàrrecs de peticions o qualsevol tipus de petició que sigui realitzada per l&#039usuari a través
de qualsevol de les formes de contacte que es posen a la seva disposició.</p>
<p>- Remetre el butlletí de notícies de la pàgina web.</p>
              <h4 class="mt-5 text-theme-colored">Criteris de conservació de les dades:</h4>
              <p>Es conservaran mentre hi hagi un interès mutu per mantenir la
fi del tractament i quan ja no sigui necessari per a tal fi, es suprimiran amb mesures de seguretat
adequades per garantir la seudonimització de les dades o la destrucció total de les mateixes.</p>
<h4 class="mt-5 text-theme-colored">Comunicació de les dades:</h4>
<p>No es comunicaran les dades a tercers, excepte per obligació legal.</p>

              <h4 class="mt-5 text-theme-colored">Drets que assisteixen a l&#039Usuari:</h4>
              <p>- Dret a retirar el consentiment en qualsevol moment.</p>
<p>- Dret d&#039accés, rectificació, portabilitat i supressió de les seves dades i de la limitació o oposició al seu
tractament.</p>
<p>- Dret a presentar una reclamació davant l&#039autoritat de control (agpd.es) si considera que el tractament
no s&#039ajusta a la normativa vigent.</p>
<h4 class="mt-5 text-theme-colored">Dades de contacte per exercir els seus drets:</h4>
<p>DONES AMB EMPENTA<br>
Adreça postal: Clos, 42 - 2º IGUALADA 08700 (BARCELONA)<br>
Correu Electrònic: donesambempenta@dae.cat</p>
<h4 class="mt-5 text-theme-colored">2. CARÀCTER OBLIGATORI O FACULTATIU DE LA INFORMACIÓ FACILITADA PER L&#039USUARI</h4>
<p>Per realitzar qualsevol tipus d&#039observació respecte a possibles incompliments dels drets de propietat
intel·lectual o industrial, així com sobre qualsevol dels continguts del lloc web, pot fer-ho a través del
correu electrònic donesambempenta@dae.cat.</p>
              <h4 class="mt-5 text-theme-colored">3. EXEMPCIÓ DE RESPONSABILITATS</h4>
              <p>Els Usuaris, mitjançant la marcació de les caselles corresponents i entrada de dades en els camps,
marcats amb un asterisc (*) en el formulari de contacte o presentats en formularis de descàrrega,
accepten expressament i de forma lliure i inequívoca, que les seves dades són necessàries per
atendre la seva petició, per part del prestador, sent voluntària la inclusió de dades en els camps
restants. L&#039Usuari garanteix que les dades personals facilitades al RESPONSABLE són veraces i es fa
responsable de comunicar qualsevol modificació de les mateixes.</p>
<p>El RESPONSABLE informa i garanteix expressament als usuaris que les seves dades personals no seran cedides en cap
cas a tercers, i que sempre que realitzés algun tipus de cessió de dades personals, es demanarà prèviament el
consentiment exprés, informat i inequívoc per part dels Usuaris. Totes les dades sol·licitades a través del lloc web són
obligatoris, ja que són necessaris per a la prestació d&#039un servei òptim a l&#039usuari. En cas que no siguin facilitades totes les
dades, no es garanteix que la informació i serveis facilitats siguin completament ajustats a les seves necessitats.</p>
              <h4 class="mt-5 text-theme-colored">3. MESURES DE SEGURETAT</h4>
              <p>Que de conformitat amb el que disposen les normatives vigents en protecció de dades personals, el
RESPONSABLE està complint amb totes les disposicions de les normatives GDPR per al tractament
de les dades personals de la seva responsabilitat, i manifestament amb els principis descrits a l&#039article
5 de l&#039GDPR, pels quals són tractats de manera lícita, lleial i transparent en relació amb l&#039interessat i
adequades, pertinents i limitats al que necessari en relació amb els fins per als quals són tractats.</p>
              <p>El RESPONSABLE garanteix que ha implementat polítiques tècniques i organitzatives apropiades per
aplicar les mesures de seguretat que estableixen l&#039GDPR per tal de protegir els drets i llibertats dels
Usuaris i els ha comunicat la informació adequada perquè puguin exercir-los.</p>';$lang['Què ens mou'] = 'Què ens mou';$lang['daetext'] = 'Dones Amb Empenta és una entitat oberta a les inquietuds i necessitats de les persones. Treballem per a generar canvis en la nostra realitat i construir una societat equitativa i lliure de violències masclistes.

<br>Tenim reconeguda la Declaració d’Utilitat Pública i garantim la màxima qualitat de les nostres activitats i serveis. Treballem amb rigor, de forma eficaç i eficient, per donar a la ciutadania una resposta integral, especialitzada, àgil, propera i coordinada. 

Treballem en xarxa i participem en els espais de construcció i reivindicació del teixit associatiu. Formem part del Consell Nacional de Dones de Catalunya.';$lang['destext'] = 'Treballem a partir de l’experiència que la societat patriarcal ens impedeix desenvolupar-nos amb drets i llibertats plenes. Cal aconseguir una societat més justa i sostenible, posant en el centre la vida i en valor la diversitat, la cura, el plaer, la cooperació. 

<br><br>Avancem per a l’eradicació de les violències masclistes mitjançant programes i serveis de formació, sensibilització, prevenció, detecció, atenció i recuperació  del dany sofert. El nostre abordatge és transversal, integral i multidisciplinar. Tenim present la dimensió social i estructural però també la subjectiva, les necessitats i els objectius de cada persona.';$lang['perstext'] = 'La nostra és una mirada àmplia i feminista. 

<br><br>Tots el nostres projectes i serveis són sensibles a les diferents formes en que les persones experimenten les situacions: mandats i creences, necessitats, oportunitats i limitacions, en funció de la seva socialització de gènere.';$lang['apodtext'] = 'Creiem en un acompanyament professional que parteix de les potencialitats. La identificació i l’enfortiment de les competències personals esdevenen clau en els processos d’apoderament de les persones. 

<br><br>Són imprescindibles la privacitat i el respecte a les individualitats, ritmes i  decisions de cada persona.  

<br><br>Donem visibilitat i valor al treball de sosteniment de la vida, el treball de cures, que entenem com a responsabilitat col·lectiva.';$lang['interdtext'] = 'Tenim en compte la diversitat de les persones, la complexitat de les situacions  i les diferents variables que la conformen. El gènere, la diversitat sexual, l’edat, l’origen, el nivell socioeconòmic, l’estat de salut específic, entre d’altres, que fan necessària una atenció individualitzada. Incorporem el repte de la mirada a la diversitat identificant i desconstruint el nostres propis prejudicis, per tal d’evitar reproduir-los en la nostra tasca.

<br><br>La sensibilitat a la diversitat, connecta amb la voluntat de generar espais de seguretat on les persones es puguin expressar, proveint l&#039oportunitat de reconèixer-se a elles mateixes i escapant de ser objectivitzades, com a primer pas cap a l&#039apoderament.';$lang['innodtext'] = 'No ens val treballar bé, sinó que volem fer-ho sempre millor. 

<br><br>Garantim la formació i actualització professional continua de les nostres  professionals. La metodologia de treball potencia la creativitat dels equips i incorpora la innovació per a una millor resposta a les demandes i necessitats de la ciutadania.

<br><br>Tenim capacitat per a crear nous serveis i per a adaptar-nos a col·lectius i territoris. ';$lang['comunidtext'] = 'Vam començar al 1996 com a espai de trobada de dones. Al llarg dels anys hem ampliat els serveis professionals i diversificat els col·lectius als que ens adrecem. Però continuem fidels al nostre origen i promovem la participació i la dinamització del teixit social i associatiu. Només amb la implicació de tots i totes podrem assolir una societat millor.
<br><br>El treball en xarxa és per a nosaltres un pilar bàsic en la intervenció professional i impulsem aquesta metodologia en tots els nostres serveis. ';$lang['Recursos econòmics'] = 'Recursos econòmics';$lang['Els recursos'] = 'Els recursos';$lang['Tot allò que ens ajuda a seguir treballant'] = 'Tot allò que ens ajuda a seguir treballant';$lang['tottext'] = 'Vetllem per la correcta administració dels recursos, amb l&#039objectiu de millorar la qualitat dels serveis, garantir l&#039eficàcia i l&#039eficiència, i la transparència en la gestió. ';$lang['Quantia de subvencions'] = 'Quantia de subvencions';$lang['Quantia de contractes'] = 'Quantia de contractes';$lang['Quantia de donacions'] = 'Quantia de donacions';$lang['quantext'] = 'Dones amb Empenta vetlla per la correcta administració dels recursos econòmics i humans, amb l&#039objectiu de millorar la qualitat dels serveis prestats, optimizar els recursos i garantir l&#039eficàcia i l&#039eficiència, així com la transparència en la gestió.';$lang['Sense documents a mostrar'] = 'Sense documents a mostrar';$lang['Resultats de la búsqueda'] = 'Resultats de la búsqueda';$lang['atenció'] = 'atenció';$lang['Menu'] = 'Menu';$lang['Informacion'] = 'Informacion';$lang['Lluitem contra la violència i discriminació'] = 'Lluitem contra la violència i discriminació';$lang['Promovem la igualtat'] = 'Promovem la igualtat';$lang['oferim'] = 'Oferim espais de trobada, de reflexió i d&#039intercanvi
                            <br>
                            A través dels nostres serveis volem erradicar la violència masclista i donar suport a les famílies i la parentalitat positiva
                            <br>
                            Treballem amb nous programes per se una entitat de referència';$lang['Atenció a les Dones'] = 'Atenció a les Dones';$lang['Interdisciplinarietat, proximitat en el territori i generalitat dels serveis. Perspectiva de gènere des d&#039una visió global i integral de les dones'] = 'Interdisciplinarietat, proximitat en el territori i generalitat dels serveis. Perspectiva de gènere des d&#039una visió global i integral
de les dones';$lang['Prevenció i sensibilització'] = 'Prevenció i sensibilització';$lang['La coeducació, l’amor romàntic i els estereotips de gènere, la violència masclista, l’educació per la salut, la sexualitat…'] = 'La coeducació, l’amor romàntic i els
estereotips de gènere, la violència masclista, l’educació per la salut, la sexualitat…';$lang['Repr. Institucional i Act. Associativa'] = 'Repr. Institucional i Act. Associativa';$lang['reptext'] = 'DAE desenvolupa un programa lúdic i cultural per promoure la participació de les dones de la comarca de l’Anoia';$lang['Formació i assessorament'] = 'Formació i assessorament';$lang['foramtext'] = 'DAE transmet el coneixement i l’experiència adquirida mitjançant la formació';$lang['Gestió i finanament'] = 'Gestió i finançament';$lang['getext'] = 'Dones Amb Empenta vetlla per la correcta administració dels recursos econòmics i humans';$lang['Altres'] = 'Altres';$lang['Altres serveis'] = 'Altres serveis';$lang['atext'] = 'Els trets essencials del model d’atenció són la interdisciplinarietat, la proximitat en el territori i la generalitat dels serveis, entenent que la intervenció es realitza amb perspectiva de gènere i des d&#039 una visió global i integral de les dones.';$lang['equd'] = 'Equip d’Atenció a la Dona (EAD)';$lang['equdtext'] = 'Des de l’ EAD s’ofereix a les dones del territori assessorament i atenció psicològica, educativa i jurídica amb perspectiva de gènere.';$lang['projecte'] = 'Projecte “Cuidar, cuidar-se”';$lang['projectext'] = 'Constitueix un espai consolidat d’acompanyament personal i suport psicològic -individual, en parella i grupal- per a la salut de les dones.';$lang['Què fem2'] = 'Què fem';$lang['territoridactuacion'] = 'Territori d&#039actuació';$lang['Zones de treball'] = 'Zones de treball';$lang['zonatext'] = 'El territori d’actuació de Dones Amb Empenta és Catalunya, essent les comarques de la Catalunya Central i de Lleida el seu espai de màxima influència. La seu de l’entitat es troba a Igualada.';$lang['Atenció a les dones2'] = 'Atenció a les dones ';$lang['Atenció espec. en viólència masclista'] = 'Atenció espec. en viólència masclista';$lang['Comarques de Lleida i Catalunya Central'] = 'Comarques de Lleida i Catalunya Central';$lang['Atencifam'] = 'Atenció i suport a les famílies ';$lang['Anoia i Osona'] = 'Anoia i Osona';$lang['Prevenció i sensibilització2'] = 'Prevenció i sensibilització';$lang['Formació, assessorament i consultoria'] = 'Formació, assessorament i consultoria';$lang['Dinamització i participació ciutadana'] = 'Dinamització i participació ciutadana';$lang['Transparència'] = 'Transparència';$lang['Entitat transparent'] = 'Entitat transparent';$lang['entitext'] = 'Som una entitat compromesa amb la necessitat de retre comptes a la ciutadania. Forma part del nostre ADN la transparència en la gestió econòmica, financera, legal i fiscal. 
                            <br>Som una entitat reconeguda d’utilitat pública i els nostres comptes anuals són supervisats per la Generalitat de Catalunya. Per mitjà d’aquesta web, fem públics els nostres valors, les nostres activitats i el seu impacte, els estatuts i número de registre, la composició dels òrgans de govern, l’organigrama i els comptes anuals.';$lang['Sin dates a mostrar en este momento.'] = 'Sin dates a mostrar en este momento.';$lang['Lloc'] = 'Lloc';$lang['Número de vacants'] = 'Número de vacants';$lang['Àrea'] = 'Àrea';$lang['Horari'] = 'Horari';$lang['Sou'] = 'Sou';$lang['Tipus de contracte'] = 'Tipus de contracte';$lang['Durada contracte'] = 'Durada contracte';$lang['Data Incorporació'] = 'Data Incorporació';$lang['Titulació'] = 'Titulació';$lang['Imprescindible'] = 'Imprescindible';$lang['Es valorarà'] = 'Es valorarà';$lang['Inscriu-te'] = 'Inscriu-te';$lang['Inscriures a'] = 'Inscriure&#039s a';$lang['Nom i cognom'] = 'Nom i cognom';$lang['Telèfon'] = 'Telèfon';$lang['Correu electrònic'] = 'Correu electrònic';$lang['Arxiu Adjunt'] = 'Arxiu Adjunt';$lang['He llegit i accepto les'] = 'He llegit i accepto les';$lang['polítiques de privacitat'] = 'polítiques de privacitat';$lang['Tancar'] = 'Tancar';$lang['Enviar'] = 'Enviar';$lang['Lluitem contra la violència i discriminació2'] = 'Lluitem contra la violència i discriminació';$lang['Promovem la igualtat2'] = 'Promovem la igualtat';$lang['protext'] = 'Oferim espais de trobada, de reflexió i d&#039intercanvi
                            <br>
                            A través dels nostres serveis volem erradicar la violència masclista i donar suport a les famílies i la parentalitat positiva
                            <br>
                            Treballem amb nous programes per se una entitat de referència
';$lang['Descarregar catàleg'] = 'Descarregar catàleg';$lang['Servei anterior'] = 'Servei anterior';$lang['Següent servei'] = 'Següent servei';$lang['Notícies2'] = 'Notícies
';$lang['Anterior'] = 'Anterior';$lang['Pàgines'] = 'Pàgines';$lang['Següent'] = 'Següent';$lang['Publicacions'] = 'Publicacions';$lang['Cartells'] = 'Cartells';$lang['Arxiu'] = 'Arxiu';$lang['Imatgens'] = 'Imatgens';$lang['Memòries'] = 'Memòries';$lang['Vídeos'] = 'Vídeos';$lang['Notícies3'] = 'Notícies';$lang['DONES AMB EMPENTA2'] = 'DONES AMB EMPENTA';$lang['llamanos'] = 'TRUCA&#039NS AL 938.045.482';$lang['Territori dactuació'] = 'Territori d&#039actuació';$lang['Lentitat en xifres'] = 'L&#039entitat en xifres';$lang['Cura de les professionals'] = 'Cura de les professionals';$lang['Actualitat'] = 'Actualitat';$lang['Contactans'] = 'Contacta&#039ns';$lang['Enviar missatge'] = 'Enviar missatge';$lang['Nom'] = 'Nom';$lang['Adreça'] = 'Adreça';$lang['Connecta amb nosaltres'] = 'Connecta amb nosaltres';$lang['Cookies'] = 'Cookies';$lang['Galeria'] = 'Galeria';