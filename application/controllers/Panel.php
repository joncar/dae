<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('registro/index/add'));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = $this->user->admin==1?'templateadmin':'template';
                    $this->load->view($template,$param);
                }                
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }        /*Cruds*/              
        
        public function index() {
            $panel = $this->user->admin==1?'panel':'panelUsuario';
            if($this->user->admin==1){
                $this->loadView($panel);
            }else{
                $crud = new ajax_grocery_crud();
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Projecte');
                $crud->set_table('proyectos');
                $crud->columns('categorias_proyectos_id','titulo','empresa','visitas');
                $crud->field_type('user_id','hidden',$this->user->id);
                $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
                $crud->field_type('status','hidden',1)
                         ->field_type('likes','hidden','[]')
                         ->field_type('visitas','hidden',0);
                $crud->display_as('empresa','Nom de l’empresa/Emprenedor')
                     ->display_as('titulo','Títol')
                     ->display_as('visitas','visites')                     
                     ->display_as('texto','Texte')
                     ->display_as('foto','Puja imatges de 770x474 pixels.')
                     ->display_as('descripcion_corta','Descripció curta (100 caràcters max)')
                     ->display_as('categorias_proyectos_id','Categoria del Projecte');
                $crud->add_action('<i class="fa fa-image"></i> Pujar fotos','',base_url('proyectos/admin/fotos_usuarios').'/');
                $crud->set_field_upload('foto','img/proyectos');                
                $crud->required_fields_array();
                $crud->unset_back_to_list();
                $crud->set_lang_string('form_save','Carregar Projecte');
                $crud->set_lang_string('insert_success_message','Projecte Carregat amb èxit <script>document.location.href="'.base_url('proyectos/admin/fotos_usuarios/{id}').'"</script>');
                $crud->unset_read()
                         ->unset_print()
                         ->unset_export();                
                $crud->where('user_id',$this->user->id);
                $crud = $crud->render();
                $crud->title = 'Panell d\'usuraris';
                $crud->scripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
                $crud->crud = 'user';
                $this->loadView($crud);
            }
        }           
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */

