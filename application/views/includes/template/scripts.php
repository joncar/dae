<script src="<?= base_url() ?>js/template/jquery-ui.min.js"></script>
<script src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/template/plugins.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc"></script>
<script src="<?= base_url() ?>js/template/map.js" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LfCUpYUAAAAAEivpX34xHEMTviClVF1KyNy4PgA"></script>
<script src="<?= base_url() ?>js/frame.js"></script>
<script src="<?= base_url() ?>js/template/theme.js"></script>
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
        jQuery(document).on('ready',function(){          
        jQuery('.social-feed-container').socialfeed({                        
            instagram:{
                accounts: ['@donesambempenta'],  //Array: Specify a list of accounts from which to pull posts
                limit: 6,         
                type:'instagram',                         //Integer: max number of posts to load
                client_id: ' b8bd91b2d691496084dd8bbaabeaba1a',       //String: Instagram client id (option if using access token)
                access_token: '4545501409.b8bd91b.46d19d513e1d46f58d4f0d41666ea8d2' //String: Instagram access token
            },facebook:{
                accounts: ['@donesambempenta'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 6,                                   //Integer: max number of posts to load
                access_token: 'EAAhcd2uyAcMBAD4pMhrS42TflQZCre5KdKuwcr9ZBtWIePW0OxZB8WZAkx1uZBRCD0DWxZBZAKOiujF7uDgJStxaRH5OeroEtnvBa6ZB5rpCNiNDZAR61IMMeqrPvmmt5sBUcchy9BvRypUPPGh0xqkNv6kaGWI4FPaK5tIrxSQ0AjInzGBO3lhJk'  //String: "APP_ID|APP_SECRET"
            },length:400,show_media:true,media_min_width: 300,update_period: 5000,template: "<?= base_url() ?>js/social/js/template.html",date_format: "ll",date_locale: "en",
            callback: function() {
                jQuery('.isotope-grid').isotope();  
                jQuery(".filter").on("click","li",function(){var e=jQuery(this),a=e.parents("ul.filter").data("related-grid");if(a){e.parents("ul.filter").find("li").removeClass("active-filt"),e.addClass("active-filt");var r=e.attr("data-filter");jQuery("#"+a).isotope({filter:r},function(){})}else alert("Please specify the dala-related-grid");return!1});                                 
                setTimeout(function(){
                    jQuery("#allSocial").trigger('click');                
                    jQuery('.isotope-grid').isotope('reloadItems');
                    jQuery('.isotope-grid').isotope('layout');
                },1000);                
            }
        });});
        jQuery(document).on('ready',function(){
            jQuery(document).on('click',".dropdown-toggle",function(){
                $(".dropdown-menu").hide();
                $(this).parents('.dropdown').find('.dropdown-menu').show();
            });
        });
</script>
<?php if(!empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>                
<?php endif; ?>
