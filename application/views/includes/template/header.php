<div id="page-preloader"><span class="spinner"></span></div>
<!-- ========================== --> 
<!-- SLIDE MENU  --> 
<!-- ========================== -->
<div off-canvas="slidebar-1 left overlay">
    <ul class=" nav navbar-nav ">
        <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
        <li><a href="#"><?= l('Notícies') ?></a></li>
        <li><a href="#"><?= l('Collaboradors') ?></a></li>   
        <li><a href="#"><?= l('Contacte') ?></a></li>                
    </ul>
</div>
<div off-canvas="mobile-slidebar left overlay">
    <ul class="nav navbar-nav">
        <li class="idiomaMobil">
            <ul>
                <li  class="idioma">
                    <a class="<?= $_SESSION['lang']=="ca"?"active":"" ?>" href="<?= base_url('main/traduccion/ca'); ?>">
                        CAT
                    </a>
                </li>
                <li  class="idioma">
                    <a class="<?= $_SESSION['lang']=="es"?"active":"" ?>" href="<?= base_url('main/traduccion/es'); ?>">
                        ESP
                    </a>
                </li>
            </ul>
        </li>
        <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
        <li class="dropdown">
            <a href="#"  class="dropdown-toggle"> <?= l('Qui som') ?> <b class="caret"></b> </a><!-- Simple  --> 
            <ul class="dropdown-menu">
                <li><a href="<?= site_url('que-ens-mou.html') ?>"> <?= l('Que ens mou') ?> </a></li>
                <li><a href="<?= site_url('lequip.html') ?>"> <?= l('Lequip') ?> </a></li>                   
                <li><a href="<?= site_url('territoridactuacio.html') ?>"> <?= l('Territori dactuació') ?> </a></li>
                <li><a href="<?= site_url('lentitatenxifres.html') ?>"> <?= l('Lentitat en xifres') ?> </a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#"  class="dropdown-toggle"> <?= l('Què fem2') ?> <b class="caret"></b> </a><!-- Simple  --> 
            <ul class="dropdown-menu">
                <?php 
                $this->db->order_by('servicios_categorias.orden','ASC');
                $serveis = $this->db->get_where('servicios_categorias'); 
                foreach($serveis->result() as $n=>$c): ?>                                                      
                    <li><a href="<?= base_url('servei/'.toURL($c->id.'-'.$c->nombre)) ?>"><?= $c->nombre ?></a></li>
                <?php endforeach ?>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#"  class="dropdown-toggle"> <?= l('Com ho fem') ?> <b class="caret"></b> </a><!-- Simple  --> 
            <ul class="dropdown-menu">
                <li><a href="<?= site_url('transparencia.html') ?>"> <?= l('Transparència') ?> </a></li>
                <li><a href="<?= site_url('recursos_economics.html') ?>"><?= l('Recursos econòmics') ?></a></li>
                <li><a href="<?= site_url('cures.html') ?>"><?= l('Cura de les professionals') ?></a></li>
                <li><a href="<?= site_url('colaboracions.html') ?>"> <?= l('Collaboracions') ?> </a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#"  class="dropdown-toggle"> <?= l('Hemeroteca') ?> <b class="caret"></b> </a><!-- Simple  --> 
            <ul class="dropdown-menu">
                <li><a href="<?= site_url('cartells') ?>"> <?= l('Cartells') ?> </a></li>
                <li><a href="<?= site_url('memories') ?>"> <?= l('Memòries') ?> </a></li>
                <li><a href="<?= site_url('videos') ?>"> <?= l('Vídeos') ?> </a></li>
                <li><a href="<?= site_url('concurs-fotografia.html') ?>"> <?= l('Concurs_Fotografic') ?> </a></li>
            </ul>
        </li>
        <li><a href="<?= base_url('events.html') ?>"><?= l('Events') ?></a></li>
        <li><a href="<?= base_url('noticies') ?>"><?= l('Notícies3') ?></a></li>
        <li><a href="<?= base_url('articles') ?>"><?= l('Articles') ?></a></li>        
        <li><a href="<?= base_url('estudis') ?>"><?= l('Estudis') ?></a></li>        
        <li><a href="<?= base_url('treballa-amb-nosaltres.html') ?>">  <?= l('Treballa amb nosaltres') ?> </a></li>
    </ul>
</div>
<div class="demo-body demo-body-2h demo-header-2 header-background-color">
    <header class="header 
            header-topbar-view
            header-normal-width
            header-background-color
            
            header-logo-white
            header-topbarbox-1-left
            header-topbarbox-2-right
            header-navibox-1-left
            header-navibox-2-right
            header-navibox-3-right
            header-navibox-4-right">
        <div class="container container-boxed-width">
            <div class="top-bar header-color-white">
                <div class="container">
                    <div class="header-topbarbox-1">
                        <ul>
                            <li><?= l('DONES AMB EMPENTA2') ?>: 
                                <a href="mailto:DONESAMBEMPENTA@DAE.CAT" style="color:inherit; text-decoration: underline;">DONESAMBEMPENTA@DAE.CAT </a>
                            </li>

                            
                        </ul>
                    </div>
                    <div class="header-topbarbox-2 hidden-xs hidden-sm">
                        <ul class="social-links">
                            <li  class="idioma">
                                <a class="<?= $_SESSION['lang']=="ca"?"active":"" ?>" href="<?= base_url('main/traduccion/ca'); ?>">
                                    CAT
                                </a>
                            </li>
                            <li  class="idioma">
                                <a class="<?= $_SESSION['lang']=="es"?"active":"" ?>" href="<?= base_url('main/traduccion/es'); ?>">
                                    ESP
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/donesambempenta/" target="_blank">
                                    <i class="social_icons fa fa-facebook-square fa-2x"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCCONPDtvfvLs4wX7FfSGdlw?view_as=subscriber" target="_blank">
                                    <i class="social_icons fa fa-youtube-square fa-2x"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/donesambempenta/?hl=es" target="_blank">
                                    <i class="social_icons fa fa-instagram fa-2x"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/DonesAmbEmpenta" target="_blank">
                                    <i class="social_icons fa fa-twitter-square fa-2x"></i>
                                </a>
                            </li>
                            <li class="no-hover">
                                <a href="tel:938053738" style="text-decoration: underline;"><?= l('llamanos') ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav id="nav" class="navbar" >
                <div class="container ">
                    <div class="header-navibox-1"> 
                        <!-- Mobile Trigger Start -->
                        <button class="menu-mobile-button visible-xs-block  js-toggle-mobile-slidebar  toggle-menu-button ">
                            <div class="toggle-menu-button-icon"> 
                                <span></span> 
                                <span></span> 
                                <span></span> 
                                <span></span> 
                                <span></span> 
                                <span></span> 
                            </div>
                        </button>
                        <!-- Mobile Trigger End--> 
                        <a class="navbar-brand scroll" href="<?= site_url() ?>"> 
                            <img class="normal-logo " src="<?= base_url() ?>img/logo.png" alt="logo"/> 
                            <img class="scroll-logo hidden-xs" src="<?= base_url() ?>img/logo.png" alt="logo"/> 
                        </a> 
                    </div>
                    
                        
                    <div class="header-navibox-3">
                        <ul class="nav navbar-nav hidden-xs clearfix vcenter">
                            <li class="">                                
                                <a class="btn_header_search" id="botonbuscar" href="#">
                                    <i class="fa fa-search"></i>
                                </a>
                                <form action="<?= base_url('paginas/frontend/search') ?>" method="get">    
                                    <input type="search" name="q" class="buscador hide" placeholder="Buscar">
                                    <button type="submit" style="display: none"></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div class="header-navibox-2">
                        <ul class="yamm main-menu nav navbar-nav">
                            <!-- Classic list -->
                            
                            <li>
                                <a href="<?= site_url() ?>"> <?= l('inici') ?> </a><!-- Simple  --> 
                            </li>
                            <li class="dropdown"> 
                                <a href="#" class="dropdown-toggle"> 
                                    <?= l('Qui som') ?><b class="caret"></b> 
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= site_url('que-ens-mou.html') ?>"> <?= l('Que ens mou') ?> </a></li>
                                    <li><a href="<?= site_url('lequip.html') ?>"> <?= l('Lequip') ?> </a></li>
                                    <li><a href="<?= site_url('lentitatenxifres.html') ?>"> <?= l('Lentitat en xifres') ?> </a></li>                                    
                                    <li><a href="<?= site_url('territoridactuacio.html') ?>"> <?= l('Territori dactuació') ?> </a></li>
                                </ul>
                            </li>
                            <li class="dropdown mega-dropdown"> 
                                <a href="#" class="dropdown-toggle"> 
                                    <?= l('Què fem2') ?><b class="caret"></b> 
                                </a>
                                <ul class="dropdown-menu mega-dropdown-menu row" style="padding-bottom:20px;">
                                    <li>
                                        
                                            <?php 
                                                $this->db->order_by('servicios_categorias.orden','ASC');
                                                $serveis = $this->db->get_where('servicios_categorias'); 
                                                foreach($serveis->result() as $n=>$c): 
                                                $c = $this->traduccion->traducirObj($c);
                                            ?>                                        
                                                    <?php if($n==0 || $n==4){echo '<div class="row">';} ?>
                                                <div class="col-sm-3">
                                                  <ul>
                                                    <li class="dropdown-header"><a style="color: #e5027d;" href="<?= base_url('servei/'.toURL($c->id.'-'.$c->nombre)) ?>"><?= $c->nombre ?></a></li>
                                                    <?php 
                                                        $this->db->order_by('servicios.orden','ASC'); 
                                                        foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>$c->id))->result() as $s): 
                                                        $s = $this->traduccion->traducirObj($s);
                                                    ?>
                                                        <li><a href="<?= base_url('serveis/'.toURL($s->id.'-'.$s->titulo)) ?>"><i class="fa fa-caret-right"></i> <?= $s->titulo ?></a></li>
                                                    <?php endforeach ?>
                                                  </ul>
                                                </div>
                                                    <?php if($n==3 || $n==$serveis->num_rows()){echo '</div>';} ?>
                                            <?php endforeach ?>
                                        </div>
                                    </li>
                                  </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle"> 
                                    <?= l('Com ho fem') ?> <b class="caret"></b> 
                                </a><!-- Simple  --> 
                                <ul class="dropdown-menu">
                                    
                                    <li><a href="<?= site_url('transparencia.html') ?>"> <?= l('Transparència') ?> </a></li>                                    
                                    <li><a href="<?= site_url('recursos_economics.html') ?>"><?= l('Recursos econòmics') ?></a></li>
                                    <li><a href="<?= site_url('cures.html') ?>"><?= l('Cura de les professionals') ?></a></li>
                                    <li><a href="<?= site_url('colaboracions.html') ?>"> <?= l('Collaboracions') ?> </a></li>
                              </ul>
                            </li>
                            <li>
                            <li class="dropdown">
                                <a href="#"  class="dropdown-toggle"> <?= l('Hemeroteca') ?> <b class="caret"></b> </a><!-- Simple  --> 
                                <ul class="dropdown-menu">
                                    <li><a href="<?= site_url('cartells') ?>"> <?= l('Cartells') ?> </a></li>
                                    <li><a href="<?= site_url('memories') ?>"> <?= l('Memòries') ?> </a></li>
                                    <li><a href="<?= site_url('videos') ?>"> <?= l('Vídeos') ?> </a></li>
                                    <li><a href="<?= site_url('concurs-fotografia.html') ?>"> <?= l('Concurs_Fotografic') ?> </a></li>
                                </ul>
                            </li>
                            <li  class="dropdown">
                                <a href="#" class="dropdown-toggle"> 
                                     <?= l('Actualitat') ?>  <b class="caret"></b>
                                </a><!-- Simple  --> 
                                <ul class="dropdown-menu">
                                    <li><a href="<?= site_url('noticies') ?>"><?= l('Notícies3') ?></a></li>                                    
                                    <li><a href="<?= base_url('events.html') ?>">  <?= l('Events') ?> </a></li>
                                </ul>
                            </li>
                            <li  class="dropdown">
                                <a href="#" class="dropdown-toggle"> 
                                     <?= l('Publicacions') ?>  <b class="caret"></b>
                                </a><!-- Simple  --> 
                                <ul class="dropdown-menu">
                                    <li><a href="<?= site_url('articles') ?>"><?= l('Articles') ?></a></li>
                                    <li><a href="<?= site_url('estudis') ?>"><?= l('Estudis') ?></a></li>                                    
                                </ul>
                            </li>
                            <li>
                                <li class="dropdown"> 
                                    <a href="#" class="dropdown-toggle"> 
                                        <?= l('Contacte') ?> <b class="caret"></b> 
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?= base_url('contacte.html') ?>"> <?= l('Contactans') ?> </a></li>
                                        <li><a href="<?= base_url('treballa-amb-nosaltres.html') ?>">  <?= l('Treballa amb nosaltres') ?> </a></li>
                                    </ul>
                                </li>
                                
                            </li>                                      
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
</div>
<script>
    $(document).on('click','#botonbuscar',function(e){
        e.preventDefault();
        if($(this).parent().parent().find('.buscador').hasClass('hide')){
            $(this).parent().parent().find('.buscador').removeClass('hide');
        }else{
            $(this).parent().parent().find('.buscador').addClass('hide');
        }

    });
</script>