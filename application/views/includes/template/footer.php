<footer class="footerBlock">
    <div class="container-bg-additional relative clearfix">
        <div class="vertical-container-top vertical-left footer-side-title hidden-md">
            <span class="vertical-text-main color-2 text-uppercase">dae</span>
            <span class="vertical-text-additional color-1 text-uppercase"><?= l('Contacte') ?></span>
            <span class="vertical-number color-1 font-primary pull-right">06</span>
        </div>
        <div class="copyright font-primary">&copy; <?= date("Y") ?> DAE</div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                    <div class="footer-form wow fadeInDown">
                        <div id="messageContact"> </div>
                        <form class="b-form-footer" method="post" onsubmit="return sendForm(this,'#respFoot')" action="paginas/frontend/contacto">
                            <h2 class="footer-form-title" style="margin-bottom: 13px"><?= l('Enviar missatge') ?></h2>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                        <input type="text" name="nom" class="form-control" id="footer-first-name" placeholder="<?= l('Nom') ?>">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input type="email" name="email" class="form-control" id="footer-mail" placeholder="<?= l('Correu electrònic') ?>">
                                    </div>
                                    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                        <input type="text" name="tema" class="form-control" id="footer-subject" placeholder="Tema">
                                    </div>
                                     <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                        <textarea id="footer-comment" name="message" class="form-control" rows="6" placeholder="Missatge" style="margin-bottom:7px; height:70px;"></textarea>
                                    </div>
                                    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 politicasCheckbox" style="margin-bottom: 40px;">
                                        <div class="checkbox">                                            
                                            <label style="color: #fff;" for="">
                                                <input type="checkbox" value="1" name="politicas" style="margin-top: 0"> 
                                                <?= l('footer-modal-politicas') ?>
                                            </label>
                                            <label style="color: #fff; margin-bottom: 10px">
                                              <?= l('politicas2-footer-contact') ?>
                                            </label>
                                            <label style="color: #fff;">
                                              <input type="checkbox" value="SI" name="extras[quiere_recibir_noticias]" style="margin-top: 0"> <?= l('footer-subscribe') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div id="respFoot"></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">                                                
                                                <div class="pull-left">
                                                    <button type="submit" class="btn btn-default-arrow">
                                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                        <?= l('Enviar') ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <div class="b-footer-right-side">
                        <div class="b-footer-contacts">
                            <div class="footer-contacts-item">
                                <h5 class="contacts-item-title">
                                    <?= l('Adreça') ?>
                                </h5>
                                <p>
                                    C/ del Clos, 42 08700 Igualada (Barcelona)
                                </p>
                            </div>
                            <div class="footer-contacts-item">
                                <h5 class="contacts-item-title">
                                    <?= l('Correu electrònic') ?>
                                </h5>
                                <p>
                                    <a href="mailto:donesambempenta@dae.cat" style="color:inherit;">
                                        donesambempenta@dae.cat
                                    </a>
                                </p>
                            </div>
                            <div class="footer-contacts-item">
                                <h5 class="contacts-item-title">
                                    <?= l('Telèfon') ?>
                                </h5>
                                <p>
                                    <a href="tel:938045482" style="color:inherit;">93 804 54 82</a>
                                </p>
                            </div>
                            <div class="b-footer-socials">
                                <ul class="list-inline">
                                    <li class="socials-title font-secondary">
                                        <?= l('Connecta amb nosaltres') ?>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/donesambempenta/">facebook</a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/donesambempenta/?hl=es">instagram</a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/DonesAmbEmpenta">twitter</a>
                                    </li>
                                    <li>
                                        <a href="https://es.linkedin.com/company/dones-amb-empenta">linkedin</a>
                                    </li>
                                </ul>
                                <ul class="list-inline" style="margin-top:53px;">                                    
                                    <li>
                                        <a href="<?= base_url('aviso_legal.html') ?>"><?= l('Avís legal') ?></a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('politica_privacitat.html') ?>"><?= l('Política de Privacitat') ?></a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('cookies.html') ?>"><?= l('Cookies') ?></a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('politica-de-xarxes-socials.html') ?>"><?= l('Política de xarxes socials') ?></a>
                                    </li>
                                    
 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-footer-bg-filter customBgColor wow fadeInRight"></div>
    </div>
    <div class="footer-btm"></div>
</footer>

<div id="contactoPoliticasModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">    
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" style="padding: 0 !important;"> <span class="tituloOferta"><?= l('Política de Privacitat') ?></span></h4>
          </div>
          <div class="modal-body">
            <?= l('footer-modal-politicas') ?>            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default-arrow btn-sm" data-dismiss="modal"><?= l('Tancar') ?></button>         
          </div>
        </div><!-- /.modal-content -->    
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->