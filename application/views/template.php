<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="es"><!--<![endif]-->
    <head>    
        <meta charset="utf-8">        
        <title><?= empty($title) ? 'Monalco' : $title ?></title>
        <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
        <meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <script>var URL = '<?= base_url() ?>';</script>
        <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>    
        <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.4/css/uikit.min.css" />
        
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">    
        <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">    
        <link href="<?= base_url() ?>css/social.css" rel="stylesheet">    
        <link rel="stylesheet" id="switcher-css" type="text/css" href="<?= base_url() ?>assets/template/switcher/css/switcher.css" media="all" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/plugins.css">
        <script src="<?= base_url() ?>js/template/jquery-2.2.3.min.js"></script>
        <?php if(!empty($css_files)): ?>
            <?php foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>        
        <?php endif; ?>
    </head> 
    <body id="page-top" class="" data-offset="90" data-target=".navbar-custom" data-spy="scroll">
        <div class="pageWrap" id="pageWrap">
            <!-- Loader -->
            <?php $this->load->view('includes/template/header'); ?>
            <?php $this->load->view($view); ?>
            <?php $this->load->view('includes/template/footer'); ?>
        </div>
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>
</html>
