<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?= site_url() ?>">
            <img src="<?= base_url() ?>img/logo-big.png" alt="" /> 
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="login-form">
            <?= !empty($msj)?$msj:'' ?>
            <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $_SESSION['email'] ?>" readonly><br/>
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Nuevo Password"><br/>
            <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Password"><br/>
            <input type="hidden" name="key" value="<?= $key ?>">
            <button type="submit" class="btn green uppercase">Enviar</button>
        </form>
        <!-- END LOGIN FORM -->
    </div>
    <div class="copyright"> 2017 © Futurmod. </div>
    <script src="<?= base_url() ?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/app.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/login.min.js" type="text/javascript"></script>
</body>
