<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getOfertas($where = array()){
		$where['publicar_hasta >='] = date("Y-m-d");
		$this->db->order_by('orden','ASC');
		$str = $this->db->get_where('ofertas',$where);
		foreach($str->result() as $n=>$o){
			$str->result_object[$n] = $this->traduccion->traducirObj($o);
			$str->row($n)->foto = base_url('img/ofertas/'.$o->foto);
			$str->row($n)->fechaabr = date("d",strtotime($o->fecha)).'-'.meses(date("m",strtotime($o->fecha)),'',$_SESSION['lang']); 
			$str->row($n)->fecha = date("d",strtotime($o->fecha)).'-'.meses(date("m",strtotime($o->fecha)),'full',$_SESSION['lang']).'-'.date("Y",strtotime($o->fecha));
		}

		return $str;
	}

	function blog($where = array()){
		$this->db->order_by('fecha','DESC');   
		$str = $this->db->get_where('blog',$where);
		foreach($str->result() as $n=>$v){
			$str->result_object[$n] = $this->traduccion->traducirObj($v);
			$str->row($n)->foto = base_url('img/blog/'.$v->foto);
			$str->row($n)->_fecha = $v->fecha;
			$str->row($n)->fecha = strftime('%d %b',strtotime($v->fecha));
			$str->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$v->id))->num_rows();                
            $str->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$v->blog_categorias_id));
            $str->row($n)->link = base_url('blog/'.toUrl($v->id.'-'.$v->titulo));
		}

		if($str->num_rows()>0){
            $str->tags = $str->row()->tags;
        }

		return $str;
	}

	function colaboradores($where = array()){		
		$str = $this->db->get_where('colaboradores',$where);
		foreach($str->result() as $n=>$v){
			$str->result_object[$n] = $this->traduccion->traducirObj($v);
			$str->row($n)->fotos = $this->db->get_where('colaboradores_fotos',array('colaboradores_id'=>$v->id));
			foreach($str->row($n)->fotos->result() as $nn=>$vv){
				$str->row($n)->fotos->row($nn)->foto = base_url('img/servicios/'.$vv->foto);	
			}
		}

		return $str;
	}

	function getEventos($where = array()){
		//$where['fecha >='] = date("Y-m-d");
		//$this->db->order_by('fecha','ASC');
		$str = $this->db->get_where('eventos',$where);
		foreach($str->result() as $n=>$o){
			$str->result_object[$n] = $this->traduccion->traducirObj($o);
			$str->row($n)->foto = base_url('img/eventos/'.$o->foto);
			$str->row($n)->fechaabr = date("d",strtotime($o->fecha)).'-'.meses(date("m",strtotime($o->fecha)),'',$_SESSION['lang']); 
			$str->row($n)->_fecha = $o->fecha;
			$str->row($n)->fechaFormat = date("d/m/Y",strtotime($o->fecha));
			$str->row($n)->fechaabr = date("d",strtotime($o->fecha)).'-'.meses(date("m",strtotime($o->fecha)),'',$_SESSION['lang']); 
			$str->row($n)->fecha = date("d",strtotime($o->fecha)).'-'.meses(date("m",strtotime($o->fecha)),'full',$_SESSION['lang']).'-'.date("Y",strtotime($o->fecha));		
			$str->row($n)->link = base_url().'event/'.$o->url;
		}

		return $str;
	}

	function contacto_telefonos($where = array()){
		$str = $this->db->get_where('contacto_telefonos',$where);
		foreach($str->result() as $n=>$o){	
			$o = $this->traduccion->traducirObj($o);		
		}

		return $str;
	}
}