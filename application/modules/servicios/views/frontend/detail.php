<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-color:lightgray;  background:url(<?= $detail->foto_banner ?>)">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= $detail->titulo ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('Què fem2') ?> </a></li>   
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= @$this->db->get_where('servicios_categorias',array('id'=>$detail->servicios_categorias_id ))->row()->nombre ?></a></li>                                     
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3" style="background: #f9f9fa !important;">
                    <div style="margin-top:50px; position:relative;">
                        <ul class="sidebarmenuserveis">

                        <?php 
                            $this->db->order_by('servicios_categorias.orden','ASC'); 
                            foreach($this->db->get_where('servicios_categorias')->result() as $c): 
                            $c = $this->traduccion->traducirObj($c);
                        ?>
                            <li><h1><a style="color: #e5027d;" href="<?= base_url('servei/'.toURL($c->id.'-'.$c->nombre)) ?>"><?= $c->nombre ?></a></h1></li>
                        <?php 
                            $this->db->order_by('servicios.orden','ASC'); 
                            foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>$c->id))->result() as $s): 
                                $s = $this->traduccion->traducirObj($s);
                            ?>
                                <li><a class="<?= $s->id==$detail->id?'active':'' ?>" href="<?= base_url('serveis/'.toURL($s->id.'-'.$s->titulo)) ?>"><i class="fa fa-caret-right"></i> <?= $s->titulo ?></a></li>
                            <?php endforeach ?>
                            </li>
                        <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9">

                    <div class="col-xs-12" style="margin-bottom: 20px; padding-bottom: 20px; border-bottom: 1px solid lightgray;">
                        <?php 
                            $categoria = $this->db->get_where('servicios_categorias',array('id'=>$detail->servicios_categorias_id ));
                            if($categoria->num_rows()>0): 
                            $c = $categoria->row();
                            $c = $this->traduccion->traducirObj($c);

                        ?>
                            <div class="col-xs-4 col-sm-2">
                                <img src="<?= base_url().'img/servicios/'.$c->icono ?>" alt="/" style="width:100%">
                            </div>
                            <div class="col-xs-8 col-sm-10 servDescr">
                                
                               <div class="b-mod-heading wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                      <p class="second-heading">
                                            <?= $c->descripcion ?>
                                      </p>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    
                    <div class="col-xs-12 col-sm-5">
                        <div class="tab-caption">
                            <div class="b-mod-heading">
                                <!--<p class="first-heading font-secondary" style="font-size:24px"><?= $detail->titulo ?></p>-->
                                <h2 class="heading-line line-right customColor customPseudoElBg" style="font-size:29px; ">
                                    <strong><?= $detail->subtitulo ?></strong>
                                </h2>
                            </div>
                            <div class="servDescr">
                                <?= $detail->texto ?>
                            </div>
                            <?php if(!empty($detail->fichero)): ?>
                                <a  style="margin: 30px 0; text-align:center;" href="<?= base_url().'files/'.$detail->fichero ?>" download class="btn btn-default-arrow btn-sm btn-clear">
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <?= l('Descarregar catàleg') ?>
                                </a>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 tab-img img-brd">
                        <div class="brd" style="z-index: 0; height:70%;"></div>
                        <ul class="list-unstyled enable-bx-slider"  data-counter="true" data-pager-custom="#bx-pager-all" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="555" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                            <li>
                                
                                <img src="<?= $detail->foto1 ?>" class="img-responsive" alt="/">
                            </li>
                            <?php if(!empty($detail->foto2)): ?><li><img src="<?= $detail->foto2 ?>" class="img-responsive" alt="/"></li><?php endif ?>
                            <?php if(!empty($detail->foto3)): ?><li><img src="<?= $detail->foto3 ?>" class="img-responsive" alt="/"></li><?php endif ?>
                            
                        </ul>

                        <div class="b-tab-additional">
                            <div class="additional-images-thumbs">
                                <ul id="bx-pager-all" class="pager-custom enable-bx-slider" data-pager-custom="null" data-controls="false" data-min-slides="2" data-max-slides="5" data-slide-width="115" data-slide-margin="22" data-pager="false" data-mode="horizontal" data-infinite-loop="false">
                                    <li>
                                        <a data-slide-index="0" href="#">
                                            <img src="<?= $detail->foto1 ?>" class="img-responsive" alt="/">
                                        </a>
                                    </li>
                                    <?php if(!empty($detail->foto2)): ?>
                                        <li>
                                            <a data-slide-index="1" href="#">
                                                <img src="<?= $detail->foto2 ?>" class="img-responsive" alt="/">
                                            </a>
                                        </li>
                                    <?php endif ?>
                                    <?php if(!empty($detail->foto3)): ?>
                                    <li>
                                        <a data-slide-index="2" href="#">
                                            <img src="<?= $detail->foto3 ?>" class="img-responsive" alt="/">
                                        </a>
                                    </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-xs-12 col-sm-12 hidden-xs">

                    </div>

                    <?php if(!empty($detail->seccion2)): ?>
                        <div class="col-xs-12 col-sm-12" style="margin: 90px 0;border-top: 1px solid lightgray;padding: 70px 0;">
                            <div class="col-xs-12 col-sm-5">
                                <img src="<?= base_url().'img/servicios/'.$detail->foto_seccion2 ?>" alt="/" style="width:100%">
                                <?php if(!empty($detail->pdf)): ?>
                                    <div style="margin: 30px 0; text-align:center;">
                                        <a href="<?= base_url().'files/'.$detail->pdf ?>" download class="btn btn-default-arrow btn-sm btn-clear">
                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                            <?= l('Descarregar catàleg') ?>
                                        </a>
                                    </div>
                                <?php endif ?>

                            </div>
                            <div class="col-xs-12 col-sm-7">
                                <h2 class="heading-line line-right customPseudoElBg" style="font-size:27px; margin-top: 0; ">
                                    <strong><?= $detail->titulo_seccion2 ?></strong>
                                </h2>
                                <?= $detail->seccion2 ?>
                            </div>
                        </div>
                    <?php endif ?>

                </div>

                <div class="col-xs-12 col-sm-12">
                    <div class="b-pagination-mod-2 center-block wow zoomIn">
                        <div class="row">
                            <?php if(!empty($prev)): ?>
                                <div class="col-xs-6 col-sm-6 text-left">
                                    <a href="<?= $prev ?>" class="pag-btn pag-prev">
                                        <i class="fa fa-long-arrow-left fa-lg" aria-hidden="true"></i> <?= l('Servei anterior') ?>
                                    </a>
                                </div>
                            <?php endif ?>
                            <?php if(!empty($next)): ?>
                                <div class="col-xs-6 col-sm-6 text-right">
                                    <a href="<?= $next ?>" class="pag-btn pag-next">
                                        <?= l('Següent servei') ?> <i class="fa fa-long-arrow-right fa-lg" aria-hidden="true"></i>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
        </div>
    </section>
</div>
