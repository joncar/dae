<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/2.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Què fem') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="01_home-1.html"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Què fem') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-services">
        <div class="container">
            
            
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-mod-heading text-center">
                        <p class="first-heading font-secondary wow fadeInDown"><?= l('Lluitem contra la violència i discriminació2') ?></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg wow fadeIn">
                            <strong><?= l('Promovem la igualtat2') ?></strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('protext') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="b-services-holder">
                <div class="row">
                    
                    <?php foreach($categorias->result() as $c): $c = $this->traduccion->traducirObj($c); ?>
                    <div class="col-xs-6 col-sm-4 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= $c->icono ?>" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= $c->nombre ?>
                                </h6>
                                <p>
                                    <?= $c->descripcion ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        
        
        
        
        
    </section>
</div>
