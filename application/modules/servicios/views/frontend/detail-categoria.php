<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-color:lightgray;  background:url(<?= $detail->foto_banner ?>)">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= $detail->nombre ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('Què fem2') ?> </a></li>  
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= $detail->nombre ?></a></li>                    
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3" style="background: #f9f9fa !important;">
                    <div style="margin-top:50px; position:relative;">
                    <ul class="sidebarmenuserveis">

                        <?php 
                            $this->db->order_by('servicios_categorias.orden','ASC'); 
                            foreach($this->db->get_where('servicios_categorias')->result() as $n=>$c):
                            $c = $this->traduccion->traducirObj($c);
                        ?>
                            <li><h1><a style="color: #e5027d;" href="<?= base_url('servei/'.toURL($c->id.'-'.$c->nombre)) ?>"><?= $c->nombre ?></a></h1></li>
                        <?php 
                            $this->db->order_by('servicios.orden','ASC'); 
                            foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>$c->id))->result() as $s): 
                            $s = $this->traduccion->traducirObj($s);
                        ?>
                                <li><a href="<?= base_url('serveis/'.toURL($s->id.'-'.$s->titulo)) ?>"><i class="fa fa-caret-right"></i> <?= $s->titulo ?></a></li>
                            <?php endforeach ?>
                            </li>
                        <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9">

                    <div class="row" style="margin-bottom: 20px; margin-left:0; margin-right:0; padding-bottom: 20px;">
                        <div class="col-xs-4 col-sm-2">
                            <img src="<?= base_url().'img/servicios/'.$detail->icono ?>" alt="/" style="width:100%">
                        </div>
                        <div class="col-xs-8 col-sm-10">
                            <div class="col-xs-8 col-sm-10 servDescr">
                                
                               <div class="b-mod-heading wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">
                                      <p class="second-heading">
                                            <?= $detail->descripcion ?>
                                      </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    


                    <?php 
                        $this->db->order_by('servicios.orden','ASC'); 
                        foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>$detail->id))->result() as $det): 
                            $det = $this->traduccion->traducirObj($det);
                    ?>
                       <div class="row" style="margin-left:0; margin-right:0; margin-bottom: 40px; padding-top:20px; border-top: 1px solid lightgray;">
                            <div class="col-xs-12 col-sm-5" style="margin-bottom:65px;">
                                <div class="tab-caption">
                                    <div class="b-mod-heading">
                                        <!--<p class="first-heading font-secondary" style="font-size:24px"><?= $detail->titulo ?></p>-->
                                        <h2 class="heading-line line-right customColor customPseudoElBg" style="font-size:28px; ">
                                            <strong><?= $det->subtitulo ?></strong>
                                        </h2>
                                    </div>
                                    <p class="tab-caption-text">
                                        <?= cortar_palabras(strip_tags($det->texto),70) ?>
                                    </p>
                                    <a href="<?= base_url() ?>serveis/<?= toUrl($det->id.'-'.$det->titulo) ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                       <?= l('llegir més') ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 tab-img img-brd">
                                <div class="brd" style="z-index: 0; height:70%;"></div>
                                <ul class="list-unstyled enable-bx-slider" data-counter="true" data-pager-custom="#bx-pager-all" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="555" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                                    <li>                                    
                                        <img src="<?= base_url('img/servicios/'.$det->foto1) ?>" class="img-responsive" alt="/">
                                    </li>
                                    
                                </ul>
                                <!--<div class="slide-counter">
                                    <strong class="current-index"></strong> / <span class="current-qty"></span>
                                </div>-->
                                
                            </div>
                        </div>
                    <?php endforeach ?>


                </div>
                
        </div>
    </section>
</div>
