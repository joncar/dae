<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function servicios_categorias(){
            $crud = $this->crud_function('','');            
            $crud->set_subject('categorias');
            //$crud->unset_delete();
            $crud->set_field_upload('foto_banner','img/servicios');
            $crud->set_field_upload('icono','img/servicios');
            $crud->field_type('fotos','gallery',array('path'=>'img/servicios','width'=>'555px','height'=>'440px'));
            $crud->columns('nombre','servicios');
            $crud->callback_column('servicios',function($val,$row){
                return '<a href="'.base_url('servicios/admin/servicios/'.$row->id).'">'.get_instance()->db->get_where('servicios',array('servicios_categorias_id'=>$row->id))->num_rows().' <i class="fa fa-plus"></i> </a>';
            });           
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/servicios_categorias/traducir').'/'); 
            $crud = $crud->render();
            $crud->title = 'Categorias de servicios';
            $this->loadView($crud);
        }
        
        function servicios($x = ''){
            $crud = $this->crud_function('','');
            //$crud->unset_delete();
            $crud->field_type('servicios_categorias_id','hidden',$x);
            $crud->where('servicios_categorias_id',$x);
            $crud->columns('titulo_corto','titulo','subtitulo');                          
            $crud->set_field_upload('foto_portada','img/servicios');
            $crud->set_field_upload('foto_banner','img/servicios');
            $crud->set_field_upload('pdf','files');
            $crud->field_type('foto1','image',array('path'=>'img/servicios','width'=>'555px','height'=>'440px'));
            $crud->field_type('foto2','image',array('path'=>'img/servicios','width'=>'555px','height'=>'440px'));
            $crud->field_type('foto3','image',array('path'=>'img/servicios','width'=>'555px','height'=>'440px'));
            $crud->field_type('foto4','image',array('path'=>'img/servicios','width'=>'555px','height'=>'440px'));
            $crud->field_type('foto_seccion2','image',array('path'=>'img/servicios','width'=>'500px','height'=>'707px'));
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/servicios/traducir').'/'); 
            $crud->set_field_upload('fichero','files');
            $crud = $crud->render();
            $this->loadView($crud);
        }



        function transparencia($x = ''){
            $crud = $this->crud_function('',''); 
            $crud->set_field_upload('fichero','files');           
            $crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'400px','height'=>'409px'))
                 ->field_type('destino','dropdown',array('1'=>'transparencia','2'=>'Cura','4'=>'Cura 2','3'=>'Recursos económicos'))
                 ->set_order('orden');
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/transparencia/traducir').'/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function colaboradores($x = ''){
            $crud = $this->crud_function('','');             
            $crud->add_action('Fotos','',base_url('servicios/admin/colaboradores_fotos').'/');
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/colaboradores/traducir').'/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function colaboradores_fotos($id){
            $crud = $this->crud_function('',''); 
            $crud->where('colaboradores_id',$id)
                 ->field_type('colaboradores_id','hidden',$id)
                 ->unset_columns('colaboradores_id')
                 ->set_order('orden');
            $crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'195px','height'=>'160px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ofertas($x = ''){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/ofertas','width'=>'555px','height'=>'300px'));
            $crud->add_action('Postulaciones','',base_url('servicios/admin/ofertas_postulaciones').'/')
                 ->columns('foto','titulo','descripcion');
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/ofertas/traducir').'/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ofertas_postulaciones($x = ''){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/ofertas','width'=>'555px','height'=>'300px'));
            $crud->where('ofertas_id',$x)
                 ->field_type('ofertas_id','hidden',$x)
                 ->unset_columns('ofertas_id')
                 ->set_field_upload('adjunto','curriculums');

            $crud = $crud->render();
            $this->loadView($crud);
        }

        function eventos(){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/eventos','width'=>'555px','height'=>'300px'));
            $crud->add_action('Inscripciones','',base_url('servicios/admin/eventos_inscritos').'/')
                 ->columns('foto','nombre');
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('servicios/admin/eventos/traducir').'/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function eventos_inscritos($x = ''){
            $crud = $this->crud_function('','');                        
            $crud->where('eventos_id',$x)
                 ->field_type('eventos_id','hidden',$x)
                 ->unset_columns('eventos_id');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function popups(){
            $crud = $this->crud_function('','');                
            $crud->field_type('foto','image',array('path'=>'img/popups','width'=>'1000px','height'=>'1414px'))
                 ->field_type('campos','editor',array('type'=>'textarea'))
                 ->callback_column('campos',function($val,$row){return strip_tags($val);})
                 ->set_clone();
            
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function contacto(){
            $crud = $this->crud_function('','');                            
            $crud->order_by('id','DESC');
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
