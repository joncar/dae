<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();   
        }     

        public function categoria($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $servicios_categorias = new Bdsource();
                $servicios_categorias->where('id',$id);
                $servicios_categorias->init('servicios_categorias',TRUE);                                                                
                $this->servicios_categorias->foto_banner = base_url('img/servicios/'.$this->servicios_categorias->foto_banner); 
                $this->servicios_categorias->fotos = explode(',',$this->servicios_categorias->fotos);
                foreach($this->servicios_categorias->fotos as $n=>$s){
                    if(!empty($s)){
                        $this->servicios_categorias->fotos[$n] = base_url('img/servicios/'.$s);
                    }
                }
                $this->servicios_categorias = $this->traduccion->traducirObj($this->servicios_categorias);
                $this->loadView(
                    array(
                        'view'=>'frontend/detail-categoria',                        
                        'detail'=>$this->servicios_categorias,
                        'title'=>$this->servicios_categorias->nombre                       
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function index(){            
            $output = array();
            $categorias = $this->db->get('servicios_categorias');
            foreach($categorias->result() as $n=>$v){
                $categorias->result_object[$n] = $this->traduccion->traducirObj($v);
                $categorias->row($n)->icono = base_url('img/servicios/'.$v->icono); 
                $categorias->row($n)->foto_banner = base_url('img/servicios/'.$v->foto_banner);                                 
                $servicios = $this->db->get_where('servicios',array('servicios_categorias_id'=>$v->id));
                $id = 0;
                $x = 0;
                $array = array();
                for($i=0;$i<$servicios->num_rows();$i++){
                    $x++;
                    $array[$id][] = $servicios->row($i);
                    if($x==3){
                        $id++;
                    }
                }
                $categorias->row($n)->servicios = $array;
            }
            $output['view'] = 'frontend/main';
            $output['title'] = 'Serveis';
            $output['categorias'] = $categorias;
            $this->loadView($output);
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $servicios = new Bdsource();
                $servicios->where('id',$id);
                $servicios->init('servicios',TRUE);                                                
                //$this->servicios->foto_portada = base_url('img/servicios/'.$this->servicios->foto_portada); 
                
                if(empty($this->servicios->foto_banner)){
                    $this->servicios->foto_banner = base_url('img/servicios/'.$this->db->get_where('servicios_categorias',array('id'=>$this->servicios->servicios_categorias_id))->row()->foto_banner);
                }else{
                    $this->servicios->foto_banner = base_url('img/servicios/'.$this->servicios->foto_banner); 
                }
                $this->servicios->foto1 = !empty($this->servicios->foto1)?base_url('img/servicios/'.$this->servicios->foto1):''; 
                $this->servicios->foto2 = !empty($this->servicios->foto2)?base_url('img/servicios/'.$this->servicios->foto2):''; 
                $this->servicios->foto3 = !empty($this->servicios->foto3) && !strpos($this->servicios->foto3,'Error')?base_url('img/servicios/'.$this->servicios->foto3):''; 
                
                $next = '';
                $prev = '';
                $enc = false;
                $this->db->order_by('servicios_categorias.orden','ASC'); 
                foreach($this->db->get_where('servicios_categorias')->result() as $c){
                    $this->db->order_by('servicios.orden','ASC'); 
                    $servicio = $this->db->get_where('servicios',array('servicios_categorias_id'=>$c->id));
                    foreach($servicio->result() as $n=>$s){
                        if(!$enc && $s->id==$this->servicios->id){
                            $enc = false;
                            $prev = !empty($servicio->result_object[$n-1])?$servicio->result_object[$n-1]:'';
                            $next = !empty($servicio->result_object[$n+1])?$servicio->result_object[$n+1]:'';

                            $prev = !empty($prev)?base_url('serveis/'.toUrl($prev->id.'-'.$prev->titulo)):'';
                            $next = !empty($next)?base_url('serveis/'.toUrl($next->id.'-'.$next->titulo)):'';
                            
                        }
                    }
                }
                
                
                

                $this->servicios = $this->traduccion->traducirObj($this->servicios);
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'next'=>$next,
                        'prev'=>$prev,
                        'detail'=>$this->servicios,
                        'title'=>$this->servicios->titulo                       
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
    }
?>
