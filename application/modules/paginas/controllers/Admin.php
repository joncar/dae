<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function paginas($action = '',$id = ''){
            switch($action){
                case 'add':
                    $this->loadView('cms/add');
                break;
                case 'insert':
                    $this->form_validation->set_rules('nombre','Nombre','required');
                    $this->form_validation->set_rules('columnas','Columnas','required|integer');
                    if($this->form_validation->run()){
                        file_put_contents('application/modules/paginas/views/'.$_POST['nombre'].'.php',$this->load->view('themes/theme'.$_POST['columnas'],array(),TRUE));
                        header("Location:".base_url('paginas/frontend/editor/'.str_replace('.php','',$_POST['nombre'])));
                        exit;
                    }else{
                        header("Location:".base_url('paginas/admin/paginas/add?msj='.urlencode('Debe llenar los datos faltantes')));
                        exit;
                    }
                break;
                case 'edit':
                    if(!empty($_POST['data']) && !empty($id)){                        
                        $_POST['data'] = str_replace('&gt;','>',$_POST['data']);
                        $_POST['data'] = str_replace('&lt;','<',$_POST['data']);
                        $_POST['data'] = str_replace('[?php','<?php',$_POST['data']);
                        $_POST['data'] = str_replace('[?=','<?=',$_POST['data']);
                        file_put_contents('application/modules/paginas/views/'.$id.'.php',$_POST['data']);
                    }
                break;
                case 'file_upload': 
                    $size = getimagesize($_FILES['image']['tmp_name']);
                    $extension = $_FILES['image']['type'];
                    $extension = explode('/',$extension);
                    $extension = count($extension>1)?$extension[1]:$extension[0];
                    $name = $id.'-'.date("dmHis").'.'.$extension;
                    if(move_uploaded_file($_FILES['image']['tmp_name'],'images/'.$name)){
                        echo json_encode(array('success'=>true,'name'=>$name,'size'=>array($size[0],$size[1])));
                    }else{
                        echo json_encode(array('success'=>false,'name'=>$name));
                    }
                break;
                case 'delete':
                    unlink('application/modules/paginas/views/'.$id);
                    redirect(base_url('paginas/admin/paginas'));
                break;
                default:
                    if(empty($action)){
                        $pages = scandir('application/modules/paginas/views');
                        $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('cms/list',array('files'=>$pages),TRUE)));
                    }
                break;
                    
            }            
        }

        function ftp(){
            $this->loadView('cms/elfinder');
        }

        function verImg($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder_img');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal_img.php';                
            }
        }

        function verFtp($connector = 0){
            if($connector==0){
                $this->load->view('cms/_elfinder');
            }else{
                require_once APPPATH.'libraries/elfinder/connector.minimal.php';                
            }
        }
        
        function banner(){
            $crud = $this->crud_function('','');            
            $crud->columns('foto');
            if($crud->getParameters()!='list'){
                $crud->field_type('foto','image',array('width'=>'600px','height'=>'300px','path'=>'img/banner'));
            }else{
                $crud->set_field_upload('foto','img/banner');
            }
            $crud->field_type('posicion','dropdown',
                    array(
                        'top:50px; left:0px;'=>'Izquierda - Arriba',
                        'top:160px; left:0px;'=>'Izquierda - Centro',
                        'bottom:0px; left:0px;'=>'Izquierda - Abajo',                        
                        'top:50px; left:30%;'=>'Centro - Arriba',
                        'top:160px; left:30%;'=>'Centro - Centro',
                        'bottom:0px; left:30%;'=>'Centro - Abajo',                        
                        'top:50px; right:0; left:initial;'=>'Derecha - Arriba',
                        'top:160px; right:0; left:initial;'=>'Derecha - Centro',
                        'bottom:0px; right:0; left:initial;'=>'Derecha - Abajo'
                    ));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria_categorias(){
            $crud = $this->crud_function('','');            
            $crud->add_action('<i class="fa fa-image"> </i> Fotos','',base_url('paginas/admin/galeria').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function xifres(){
            $crud = $this->crud_function('','');                        
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('galeria')
                     ->set_relation_field('galeria_categorias')
                     ->set_title_field('titulo')
                     ->set_url_field('foto')
                     ->set_image_path('img/galeria')
                     ->set_ordering_field('priority');
            $crud->module = 'paginas';
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function categoria_multimedia(){
            $crud = $this->crud_function('','');     
            $crud->field_type('tipo','enum',array('cartells','memories','videos','concurso de fotografia','Galeria','Estudis'));
            $crud->field_type('idiomas','hidden')
                 ->unset_columns('idiomas')
                 ->set_order('orden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('paginas/admin/categoria_multimedia/traducir').'/'); 
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function multimedia(){
            $crud = $this->crud_function('','');            
            $crud->field_type('tipo','dropdown',array('1'=>'Cartells','2'=>'Memories','3'=>'Videos','5'=>'Galeria','6'=>'Estudis'));
            $crud->set_relation('categoria_multimedia_id','categoria_multimedia','{tipo}-{nombre}');
            $crud->set_field_upload('url','files')
                 ->field_type('portada','image',array('path'=>'files','width'=>'800px','height'=>'1149px'))
                 ->display_as('url','Fichero');
            $crud->set_order('orden');
            $crud->field_type('lugar','hidden')
                 ->unset_columns('autor','titulo','lugar')
                 ->where('multimedia.tipo !=',4)
                 ->where('multimedia.tipo !=',3);
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function videos(){
            $this->as['videos'] = 'multimedia';
            $crud = $this->crud_function('','');            
            $crud->field_type('tipo','hidden',3);
            $crud->field_type('categoria_multimedia_id','hidden');
            $crud->set_field_upload('url','files')
                 ->field_type('portada','image',array('path'=>'files','width'=>'457px','height'=>'350px'))
                 ->display_as('url','Fichero');
            $crud->set_order('orden');
            $crud->field_type('lugar','hidden')
                 ->unset_columns('autor','titulo','lugar')
                 ->where('multimedia.tipo',3);
            $crud = $crud->render();
            $this->loadView($crud);   
        }

        function concurso_fotografia(){
            $this->as['concurso_fotografia'] = 'multimedia';
            $crud = $this->crud_function('','');            
            $crud->field_type('tipo','hidden','4')                 
                 ->field_type('portada','image',array('path'=>'files','width'=>'1092px','height'=>'800px'))
                 ->set_field_upload('video','files')
                 ->set_relation('categoria_multimedia_id','categoria_multimedia','{tipo}-{nombre}',array('tipo'=>'concurso de fotografia'))            
                 ->display_as('portada','Miniatura')
                 ->display_as('url','Texto')
                 ->display_as('video','Foto')
                 ->where('multimedia.tipo','4')
                 ->columns('categoria_multimedia_id','portada','url');
            $crud->set_order('orden');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function contacto_telefonos(){            
            $crud = $this->crud_function('','');
            $crud->field_type('idiomas','hidden')
                 ->unset_columns('idiomas')
                 ->add_action('Traducir','',base_url('paginas/admin/contacto_telefonos/traducir').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function equipo($x = ''){
            $crud = $this->crud_function('','');            
            $crud->field_type('foto','image',array('path'=>'img/servicios','width'=>'262px','height'=>'260px'));
            $crud->columns('foto','nombre','cargo');
            if($x!='traducir'){
                $crud->callback_field('areas',function($val){
                    $s = '<input type="hidden" name="areas" id="field-areas" value="'.$val.'">';
                    $l = '<div class="row rowe">
                        <div class="col-xs-4">
                            <input type="text" class="colorName" placeholder="Nombre" value="[nombreValue]">
                        </div>
                        <div class="col-xs-8">
                            <input type="text" class="colorHex" placeholder="Servei-Programa (Separado por coma)" value="[colorValue]">
                            <a href="javascript:addRow()" style="color:green">
                                <i class="fa fa-plus-circle"></i>
                            </a>
                            <a href="javascript:void(0)" onclick="removeRow(this)" style="color:red">
                                <i class="fa fa-minus-circle"></i>
                            </a>
                        </div>
                    </div>';
                    if(empty($val)){                    
                        $l = str_replace('[nombreValue]','',$l);
                        $l = str_replace('[colorValue]','',$l);
                    }else{
                        $colores = explode('--',$val);
                        $p = '';
                        foreach($colores as $c){
                            if(!empty($c)){                            
                                $pp = $l;
                                if(strpos($c,':')>=0){
                                    list($nombre,$color) = explode(':',$c);
                                    $c = $color;
                                    $pp = str_replace('[nombreValue]',$nombre,$pp);
                                }
                                $pp = str_replace('[colorValue]',$c,$pp);
                                $p.= $pp;
                            }
                        }
                        $l = $p;
                    }
                    return $s.$l;
                });
            }else{
                $crud->callback_field('areas',function($val){
                    $s = '<input type="text" name="areas" id="field-areas" value="'.$val.'">';
                    return $s;
                });
            }

            $crud->set_order('orden');
            $crud->set_field_upload('ficha','files');
            $crud->field_type('idiomas','hidden');
            $crud->unset_columns('idiomas');
            $crud->add_action('Traducir','',base_url('paginas/admin/equipo/traducir').'/');
            $crud = $crud->render();
            $crud->output = $this->load->view($this->theme.'areas',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }
    }
?>
