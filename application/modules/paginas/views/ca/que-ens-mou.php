<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/8.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Que ens mou') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Qui som') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Que ens mou') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">            
                <div class="row" style=" margin-bottom: 60px;">
                <div class="col-xs-12 col-md-6">
                    <div class="b-mod-heading wow fadeInDown">
                        <p class="first-heading font-secondary"><?= l('Dones amb Empenta') ?> </p>
                        <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong>DAE</strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('daetext') ?>

                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="img-brd-mod">
                        <div class="brd"></div>
                        <div class="img-cut">
                            <div class="cut"></div>
                            <img src="<?= base_url() ?>assets/template/media/about/about1.jpg" class="img-responsive center-block" alt="/">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="b-services-holder clearfix">
                    <div class="col-xs-12 col-sm-6 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/V.png"style=" vertical-align: bottom;" alt="/"></i> <?=  l('Desigualtats i Violències'); ?>
                                </h5>
                                <p>
                                    <?= l('destext') ?>


                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/p.png"style=" vertical-align: bottom;" alt="/"></i> <?= l('PERSPECTIVA DE GÈNERE') ?>
                                </h5>
                                <p>
                                    <?= l('perstext') ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="b-services-holder clearfix">
                    <div class="col-xs-12 col-sm-6 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/a.png"style=" vertical-align: bottom;" alt="/"></i> <?= l('APODERAMENT') ?>
                                </h5>
                                <p>
                                    <?= l('apodtext') ?>


                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/i.png"style=" vertical-align: bottom;" alt="/"></i> <?= l('INTERSECCIONALITAT') ?>
                                </h5>
                                <p>
                                    <?= l('interdtext') ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="b-services-holder clearfix">
                    <div class="col-xs-12 col-sm-6 wow slideInLeft">
	                        <div class="b-services-item">
	                            <div class="services-text">
	                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
	                                    <img src="<?= base_url() ?>assets/template/media/services-icons/f.png" style=" vertical-align: bottom;"alt="/"></i> <?= l('INNOVACIÓ') ?>

	                                </h5>
	                                <p>
	                                    <?= l('innodtext') ?>
	                                </p>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-xs-12 col-sm-6 wow slideInRight">
	                        <div class="b-services-item">
	                            <div class="services-text">
	                                <h5 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
	                                    <img src="<?= base_url() ?>assets/template/media/services-icons/comunity.png"style=" vertical-align: bottom;" alt="/"></i> <?= l('COMUNITAT') ?>
	                                </h5>
	                                <p>
	                                    <?= l('comunidtext') ?>
</p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </div>
        </div>
    </section>
</div>
