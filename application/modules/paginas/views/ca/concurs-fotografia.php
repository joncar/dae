<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/48.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Concurs_Fotografia') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('Hemeroteca') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Concurs_Fotografia') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-items-works text-uppercase text-center clearfix">
                        <ul class="list-inline b-items-sort">
                            
                            <li class="active-filt" data-filter="*">
                                <?= l('Tots') ?>
                            </li>

                            <?php foreach($this->db->get_where('categoria_multimedia',array('tipo'=>'concurso de fotografia'))->result() as $c): $c = $this->traduccion->traducirObj($c); ?>
                                <li class="" data-filter=".c<?= $c->id ?>">
                                    <?= $c->nombre ?>
                                </li>
                            <?php endforeach ?>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="b-works-holder text-center"  uk-lightbox>

                            <?php 
                                $this->db->order_by('orden','ASC');
                                foreach($this->db->get_where('multimedia',array('tipo'=>4))->result() as $f): 
                            ?>
                                <div class="b-member-caption cartells works-item c<?= $f->categoria_multimedia_id ?>" style="margin-bottom:0; width:auto;">
                                    <?php if(!empty($f->url)): ?>
                                    <div style="position:relative;">
                                        <div style="position: absolute;z-index: 10000;background: #e50d7e;width: 50%;height: auto;color: #fff;top: -10px;right: -10px;font-size: 12px;text-align: center;padding: 12px;font-weight: 800;text-transform: uppercase;white-space: nowrap;"><?= $f->url ?></div>
                                    </div>
                                    <?php endif ?>
                                    <a class="imageWork2 workImg contact-info-title" href="<?= base_url('files/'.$f->video) ?>" data-width="100" style="height:auto">                                        
                                        <h6 class="contact-info-title customPseudoElBg font-secondary" style="text-transform: none;">
                                            <img src="<?= base_url('files/'.$f->portada) ?>" class="img-responsive center-block" alt="/">                                            
                                            <span style="margin-top:10px; display: block; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><?= $f->titulo ?></span>
                                            <span style="font-weight: 300;color: #999;font-size: 12px; display:block; margin-top:15px; display: block;">
                                                <?= $f->autor ?><br>
                                                <?= $f->lugar ?>
                                            </span>
                                        </h6>                                        
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
