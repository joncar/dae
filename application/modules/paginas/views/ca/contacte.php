<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/2.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Contacto') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici')?></a></li> 
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Contacte') ?></span></li>  
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Contacto') ?></span></li>                 
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
 

   <section class="section-contact-2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-mod-heading text-center wow fadeInDown">
                        <p class="first-heading font-secondary"></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong><?= l('Posacontacte_amb_nosaltres') ?></strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('Necesita_Informacion') ?>
                        </p>
                    </div>

                    <div class="b-table table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>
                                        <span class="customPseudoElBg">#</span>
                                    </td>
                                    <td>
                                        <span class="customPseudoElBg"><?= l('servicio') ?></span>
                                    </td>
                                    <td>
                                        <span class="customPseudoElBg"><?= l('Telèfon') ?></span>
                                    </td>
                                    <td>
                                        <span class="customPseudoElBg">Email</span>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach($this->elements->contacto_telefonos()->result() as $n=>$c): ?>
                                    <tr>
                                        <td>
                                            <?= $n+1 ?>
                                        </td>
                                        <td>
                                            <?= $c->nombre ?>
                                        </td>
                                        <td>
                                            <?= $c->telefono ?>
                                        </td>
                                        <td>
                                            <a href="mailto:<?= $c->email ?>"><?= $c->email ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <div class="b-home-video-block">
            <div class="container-fluid">
                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 left-side wow slideInLeft">
                    
                </div>
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 right-side wow slideInRight">
                    <div class="content">
                        <div class="b-mod-heading">
                            <p class="first-heading customPseudoElBg font-secondary"><?= l('Colaboracion') ?></p>
                            <h2 class="heading-line line-right">
                                <strong><?= l('amb_nosaltres') ?></strong>
                            </h2>
                            
                            <br/>
                            <a href="<?= base_url() ?>treballar.html" class="btn btn-default-arrow btn-sm btn-clear">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                <?= l('ofertes_treball') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</div>
