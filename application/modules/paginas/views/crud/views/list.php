<?php if(!empty($list)): ?>
    <?php foreach($list as $num_row => $p): ?>    
         <div class="post">
                <div class="wrap-post">
                    <div class="img-recent-causes">
                        <a href="<?= $p->link ?>"><img src="<?= $p->foto ?>" alt="image"></a>
                    </div>
                    <div class="post-recent-causes">
                        <div class="title-later-new">
                            <p> <?= $p->s379c07ff ?></p>
                        </div> 
                        <h6 class="title-post">
                            <?php $ponts = strlen($p->titulo)>46?'...':''; ?>
                            <a href="<?= $p->link ?>"><?= substr($p->titulo,0,46).$ponts ?></a>
                        </h6>
                    </div>
                   <div class="entry-content">
                        <div class="donation-v1">
                            <p><?= substr(strip_tags($p->descripcion_corta),0,255) ?></p><span></span>
                        </div>

                    </div>
                    <div class="flat-progress">
                        <div class="perc"><?= $p->empresa ?></div>
                        <div class="progress-bar" data-percent="100" data-waypoint-active="yes">
                            <div class="progress-animate"></div>
                        </div>
                    </div><!-- /flat-progress -->
                    <div class="more">
                        <a><i class="icon-eye"></i>  <?= $p->visitas ?> vistas</a>
                        <a id='likes<?= $p->id ?>' href="javascript:sumarCorazon('<?= $p->id ?>')"><i class="icon-heart"></i>  <?= $p->likes ?></a>
                        <button onclick="document.location.href='<?= $p->link ?>'" class="flat-button button-style">Veure Projecte</button>
                    </div>
                </div>                        
        </div>
    <?php endforeach ?>     
<?php endif; ?>
