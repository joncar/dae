<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function lista($tipo){                    
        	if(is_numeric($tipo)){
        		switch($tipo){
        			case '1':
        				$name = 'cartells';
                        $title = l('Cartells');
        			break;
        			case '2':
        				$name = 'memories';
                        $title = l('Memòries');
        			break;
        			case '3':
        				$name = 'videos';
                        $title = l('Videos');
        			break;
                    case '5':
                        $name = 'galeria';
                        $title = l('Galeria');
                    break;
                    case '6':
                        $name = 'estudis';
                        $title = l('Estudis');
                    break;
        		}
            	$this->loadView(array('view'=>$name,'title'=>$title));
        	}
        }
    }
?>
