<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/44.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Estudis') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('Publicacions') ?> </a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Estudis') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-items-works text-uppercase text-center clearfix">
                        <ul class="list-inline b-items-sort">
                            
                            <li class="active-filt" data-filter="*">
                                <?= l('Totes') ?>
                            </li>

                            <?php 
                            $this->db->order_by('orden','ASC');
                            foreach($this->db->get_where('categoria_multimedia',array('tipo'=>'estudis'))->result() as $c): 
                                $c = $this->traduccion->traducirObj($c);
                            ?>
                                <li class="" data-filter=".c<?= $c->id ?>">
                                    <?= $c->nombre ?>
                                </li>
                            <?php endforeach ?>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="b-works-holder text-center">

                            <?php 
                                $this->db->order_by('orden','ASC');
                                foreach($this->db->get_where('multimedia',array('tipo'=>6))->result() as $f): 
                            ?>
                                <div class="cartells works-item c<?= $f->categoria_multimedia_id ?>">
                                    <a class="imageWork2 workImg" href="<?= base_url('files/'.$f->url) ?>" target="_new">
                                        <img src="<?= base_url('files/'.$f->portada) ?>" class="img-responsive center-block" alt="/">
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
