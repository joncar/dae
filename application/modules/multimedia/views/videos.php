<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/45.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Vídeos') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('Hemeroteca') ?> </a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Vídeos') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-items-works text-uppercase text-center clearfix">
                        <ul class="list-inline b-items-sort">
                            
                            <li class="active-filt" data-filter="*">
                                <?= l('Tots') ?>
                            </li>

                            <?php 
                                $this->db->order_by('orden','ASC');
                                foreach($this->db->get_where('categoria_multimedia',array('tipo'=>'videos'))->result() as $c): 
                                    $c = $this->traduccion->traducirObj($c);
                            ?>
                                <li class="" data-filter=".c<?= $c->id ?>">
                                    <?= $c->nombre ?>
                                </li>
                            <?php endforeach ?>

                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="b-works-holder text-center b-member-caption" uk-lightbox >

                            <?php 
                                $this->db->order_by('orden','ASC');
                                foreach($this->db->get_where('multimedia',array('tipo'=>3))->result() as $f): 
                            ?>
                                <div class="cartells works-item c<?= $f->categoria_multimedia_id ?> contact-info-title">
                                    <a class="imageWork1" href="<?= $f->video ?>" type="iframe" lightbox-type="iframe">
                                        <img src="<?= base_url('files/'.$f->portada) ?>" class="img-responsive center-block" alt="/">
                                        <span style="margin-top:10px; display: block; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><?= $f->titulo ?></span>
                                        <span style="font-weight: 300;color: #999;font-size: 12px; display:block; margin-top:15px; display: block;"><?= $f->autor ?></span>
                                    </a>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
