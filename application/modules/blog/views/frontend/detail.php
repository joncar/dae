<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/22.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= $detail->titulo ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>">home</a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="<?= site_url('blog') ?>"><?= $type ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= $detail->titulo ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-blog-post">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-blog-post">
                        <div class="b-blog-data text-right hidden-xs">
                            <span class="blog-data-category font-secondary text-uppercase"></span>
                            <span class="blog-data-date font-primary text-uppercase">
                                <?= date("d",strtotime($detail->fecha)).meses(date("m",strtotime($detail->fecha)),'short',$_SESSION['lang']); ?>
                            </span>
                        </div>
                        <div class="b-blog-caption wow fadeInDown">
                            <div class="caption-data customPseudoElBg">
                                <span class="date">
                                    <?= date("d",strtotime($detail->fecha)).'-'.meses(date("m",strtotime($detail->fecha)),'full',$_SESSION['lang']).'-'.date("Y",strtotime($detail->fecha)); ?>        
                                </span>
                                <span class="author"><?= $detail->user ?></span>
                            </div>
                        </div>
                        <div>
                            <div class="b-blog-post-img img-cut" style="float:left; width:40%; margin: 0 30px 24px; margin-left: 0">
                                <div class="cut"></div>
                                <img src="<?= $detail->foto ?>" class="img-responsive center-block" alt="/">
                            </div>
                            <div>
                                <?= $detail->texto ?>
                            </div>
                        </div>
                        <div class="b-post-tags clearfix">
                            <div class="pull-left wow slideInLeft">
                                <ul class="list-inline tags-list">
                                    <li class="contact-info-title customPseudoElBg font-secondary">post tags</li>
                                    <?php if(!empty($detail->tags)): ?>
                                        <?php foreach(explode(',',$detail->tags) as $t): ?>
                                            <li><a href="#"><?= $t ?></a></li>/ 
                                        <?php endforeach ?>            
                                    <?php endif ?>
                                </ul>
                            </div>
                            <div class="pull-right wow slideInRight">
                                <ul class="list-inline post-share">
                                    <li>
                                        <a href="https://www.facebook.com/">facebook</a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/">twitter</a>
                                    </li>
                                    <li>
                                        <a href="https://plus.google.com/">google +</a>
                                    </li>
                                    <li>
                                        <a href="https://pinterest.com">pinterest</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="b-pagination-holder">
                            <div class="b-pagination-mod-2 center-block">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 text-left">
                                        <a href="<?= $prev ?>" class="pag-btn pag-prev">
                                            <i class="fa fa-long-arrow-left fa-lg" aria-hidden="true"></i> ANTERIOR
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 text-right">
                                        <a href="<?= $next ?>" class="pag-btn pag-next">
                                            POSTERIOR <i class="fa fa-long-arrow-right fa-lg" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
