<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/22c.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Publicacions') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Actualitat') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Publicacions') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-blog">
        <div class="container">
            <div class="b-mod-heading text-center wow fadeInDown" style="visibility: visible; animation-name: fadeInDown; margin-bottom:100px;">
                <p class="second-heading font-additional">
                    <?= l('publicacions_intro') ?>
                </p>
            </div>
            <div class="row">
                
                <?php foreach($detail->result() as $d): ?>
                    <div class="b-blog-item clearfix wow slideInLeft">
                        <div class="b-blog-data text-right hidden-xs">
                            <span class="blog-data-category font-secondary text-uppercase"></span>
                            <span class="blog-data-date font-primary text-uppercase">
                                <?= date("d",strtotime($d->fecha)).meses(date("m",strtotime($d->fecha)),'short',$_SESSION['lang']); ?>
                            </span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="b-blog-img img-cut">
                                <div class="cut"></div>
                                <?php if(!empty($d->foto)): ?>                                        
                                    <img src="<?= $d->foto ?>" class="img-responsive center-block" alt="/">
                                <?php endif ?>                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="b-blog-caption">
                                <div class="caption-data customPseudoElBg">
                                    <span class="date"><?= date("d",strtotime($d->fecha)).'-'.meses(date("m",strtotime($d->fecha)),'full',$_SESSION['lang']).'-'.date("Y",strtotime($d->fecha)); ?>       </span>                                    
                                </div>
                                <h4 class="caption-title">
                                    <?= $d->titulo ?>
                                </h4>
                                <div class="caption-preview-text">
                                    <?= substr(strip_tags($d->texto),0,255).'...' ?>
                                </div>
                                <a href="<?= $d->link ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <?= l('llegir més') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
                
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 wow zoomIn">
                    <div class="b-pagination-mod-1">
                        <div class="row">
                            <div class="col-xs-3 col-sm-4 text-left">
                                <?php if(!empty($_GET['page']) && $_GET['page'] > 1): ?>
                                <a href='<?php base_url('blog') ?>?page=<?= empty($_GET['page'])?1:($_GET['page']-1) ?>' class="pag-btn pag-prev">
                                    <i class="fa fa-long-arrow-left fa-lg" aria-hidden="true"></i> Anterior
                                </a>
                                <?php endif ?>
                            </div>
                            <div class="col-xs-6 col-sm-4 text-center">
                                <div class="b-current-pag">
                                    <p>Pàgines &ensp;
                                        <span class="pag-current"><?= empty($_GET['page'])?1:($_GET['page']) ?></span>
                                        <span class="pag-divider">/</span>
                                        <span class="pag-total"><?= $total_pages ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-4 text-right">
                                <?php if(!empty($_GET['page']) && $_GET['page'] < $total_pages): ?>
                                <a href='<?php base_url('blog') ?>?page=<?= empty($_GET['page'])?1:($_GET['page']+1) ?>' class="pag-btn pag-next">
                                    Següent <i class="fa fa-long-arrow-right fa-lg" aria-hidden="true"></i>
                                </a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
