<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->field_type('foto','image',array('path'=>'img/blog','width'=>'1070px','height'=>'600px'));
            $crud->field_type('tags','tags');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->field_type('idiomas','hidden');            
            $crud->add_action('<i class="fa fa-world"></i> Traducir','',base_url('b/blog/traducir').'/');
            $crud->field_type('user','string',$this->user->nombre.' '.$this->user->apellido);
            $crud->columns('blog_categorias_id','titulo','status','fecha');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function blog_categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria');
            $crud->display_as('blog_categorias_nombre','Nombre');
            $crud = $crud->render();
            $crud->title = 'Categorias';
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
